#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "LocalizationSystem.h"

FOUNDATION_EXPORT double AMLocalizedVersionNumber;
FOUNDATION_EXPORT const unsigned char AMLocalizedVersionString[];

