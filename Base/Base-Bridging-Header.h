//
//  Base-Bridging-Header.h
//  Base
//
//  Created by Hamza Khan on 24/11/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//


#import "MMDrawerController.h"
#import "MMNavigationController.h"
#import "MMDrawerVisualState.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "UIViewController+MMDrawerController.h"
#import "UIView+DCAnimationKit.h"
#import "XMLReader.h"
#import <CommonCrypto/CommonCrypto.h>


#define VC_SPLASH               "SplashViewController"
#define VC_LEFT                 "LeftMenuViewController"
#define VC_DASHBOARD            "DashboardViewController"

#define VC_RECTOR_MSG           "RectorMsgViewController"
#define VC_UNI_NEWS             "NewsViewController"
#define VC_UNI_EVENTS           "EventsViewController"
#define VC_UNI_HISTORY          "UniversityHistoryViewController"
#define VC_EMPLOYEE_DIRECTORY   "EmployeDirectoryViewController"
#define VC_UNI_MAP              "MapViewController"

#define VC_USER_LOGIN           "LoginViewController"
#define VC_SOCIAL_NETWORK       "SocialNetworkViewController"
#define VC_CONTACT_INFO         "ContactUsViewController"

#define VC_ADMIN_SERVICES       "AdministrativeServicesViewController"
#define VC_ICT_SERVICES         "ICTServicesViewController"
#define VC_ACADEMIC             "AcademicViewController"

#define VC_SALARY_SLIP          "SalarySlipViewController"
#define VC_DEPUTATION_INQUIRY   "DeputationInquiryViewController"
#define VC_DEPUTATION_DETAIL    "DeputationDetailViewController"
#define VC_OVERTIME_INQUIRY     ""
#define VC_VACATION_INQUIRY     "VocationInquiryViewController"
#define VC_WEBVIEW              "WebViewViewController"
#define VC_VACATION_DETAIL      "VacationDetailViewController"

//STUDENT PAGES

#define VC_PROFILE              "PersonalInformationViewController"
#define VC_TIMETABLE            "TimeTableViewController"
#define VC_GRADES               "TermGradesViewController"
#define VC_ATTENDANCE           "AttendanceViewController"
#define VC_ACCOUNT              "StudentAccountViewController"
#define VC_EVENTS               "EventsViewController"
#define VC_TODO                 "TodoListViewController"
#define VC_ADVISOR              "AdvisorInformationViewController"
#define VC_INDICATOR            "ServiceIndicatorViewController"
#define VC_ENROLLMENT           "EnrollmentViewController"
#define VC_ABOUT_UNI            "AboutUniViewController"

#define VC_CHOOSE_LAGUAGE       "ChooseLanguageViewController"
#define VC_WEB_VIEW             "WebViewViewController"
#define VC_OVERTIME             "OvertimeViewController"
#define VC_SELECT_TERM          "SelectTermViewController"
#define VC_SELECT_SUBJECT       "SelectSubjectViewController"
#define VC_SHOPPING_CART        "ShoppingCartViewController"
#define VC_COURSE_INQUIRY       "CourseInquiryViewController"
#define VC_COURSE               "CourseViewController"
#define VC_SELECT_CLASS         "SelectClassViewController"
#define VC_CLASS_DETAIL         "ClassDetailViewController"

#define VC_HARDWARE_REQUEST     "HardwareRequestViewController"
#define VC_CLEARANCE_REQUEST    "ClearanceRequestViewController"
#define VC_LEAVE_REQUEST        "LeaveRequestViewController"
#define VC_BOOKING_REQUEST      "BookingRequestViewController"
#define VC_DEPUTATION_REQUEST   "DeputationRequestViewController"
#define VC_GENERAL_REQUEST      "GeneralRequestViewController"
#define VC_ENGAGEMENT_REQUEST   "EngagementRequestViewController"
#define VC_SOFTWARE_REQUEST     "SoftwareRequestViewController"
#define VC_MAINTENANCE_REQUEST  "MaintenanceRequestViewController"

#define VC_NEWS_DETAIL          "NewsDetailViewController"
