//
//  AppDelegate.swift
//  Base
//
//  Created by Noman Siddiqui on 24/11/2016.
//  Copyright © 2016 Noman Siddiqui. All rights reserved.
//

import UIKit
import AMLocalized
import GoogleMaps


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let single = Singleton.sharedInstance
    var window: UIWindow?
    var drawerController: MMDrawerController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        //LocalizationSystem.sharedLocal().setLanguage(constants.basicKeywords.arabicLanguage)
        
        //language.setLanguage(type: ChooseLanguageTypeEnum.kLanguageEnglish)
        setupNavController()
        GMSServices.provideAPIKey("AIzaSyDjVZYIqn4_95_c1GRHg6pndQcxsZoKJ3U")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func setupNavController()
    {
        let vcBase = VC_SPLASH
        
        let sB = navigation.getStoryBoardForController(identifier: vcBase)?.storyboardObject
        let initialVC : UIViewController = (sB!.instantiateViewController(withIdentifier: vcBase))
        
        let navController = MMNavigationController(rootViewController: initialVC)
        navController.restorationIdentifier = vcBase
        navController.setNavigationBarHidden(true , animated: false)
        
        
        let leftSideNavController = createSideMenuViewControllerFromStoryBoard(vcIdentifier: VC_LEFT)
        //let rightSideNavController = createSideMenuViewControllerFromStoryBoard(vcIdentifier: RIGHT_VC)
        
        drawerController = MMDrawerController(center: navController, leftDrawerViewController: leftSideNavController, rightDrawerViewController: nil)
        
        drawerController?.showsShadow = true
        drawerController?.openDrawerGestureModeMask = .all
        drawerController?.closeDrawerGestureModeMask = .all
        //  drawerController?.centerHiddenInteractionMode = .Full
        //drawerController?.maximumLeftDrawerWidth = leftMenu .getMenuWidth()
        //  drawerController?.centerHiddenInteractionMode = .Full
        
        var dwWidth = UIScreen.main.bounds.width * 0.9 // * 0.845410628
        
        if GlobalStaticMethods.isPad(){
            dwWidth =   DeviceUtil.size.width * 0.9// * 0.38251
        }
        
        drawerController?.maximumLeftDrawerWidth = dwWidth
        drawerController?.shouldStretchDrawer = false
        drawerController?.setDrawerVisualStateBlock({ (drawerController, drawerSide, percentVisible) in
            let block :  MMDrawerControllerDrawerVisualStateBlock
            block = MMExampleDrawerVisualStateManager .shared().drawerVisualStateBlock(for: drawerSide)
            //if block == true {
            
            block(drawerController, drawerSide, percentVisible)
            
            //}
            
        })
        
        self.window?.rootViewController = self.drawerController
        self.window?.makeKeyAndVisible()
        
        
    }
  
    func createSideMenuViewControllerFromStoryBoard(vcIdentifier : String) -> UINavigationController {
      
        
        
        let sB = navigation.getStoryBoardForController(identifier: vcIdentifier)!.storyboardObject
        //--ww let sB = UIStoryboard(name: "waqas-ipad-en", bundle: nil)
        let sideDrawerViewController = sB.instantiateViewController(withIdentifier: vcIdentifier)
        
        let sideNavController = MMNavigationController(rootViewController: sideDrawerViewController)
        sideNavController .setNavigationBarHidden(true, animated: true)
        
        return sideNavController
        
    }

    
//    func application(_ application: UIApplication,
//                     supportedInterfaceOrientationsFor window: UIWindow?)
//        -> UIInterfaceOrientationMask {
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                return .landscape
//            } else {
//                return .portrait
//            }
//    }

    public func resetLeftMenu()
    {
        let leftSideNavController = createSideMenuViewControllerFromStoryBoard(vcIdentifier: VC_LEFT)
        self.drawerController?.leftDrawerViewController = leftSideNavController
    }
    
    func setUDID() {
        let userDefaults = UserDefaults.standard
        
        if (userDefaults.string(forKey: "UDID") != nil){
            
            single.deviceToken = userDefaults.string(forKey: "UDID")!
        }
    }

    
    
}

