//
//  TimeTableScrollView.swift
//  TimeTable
//
//  Created by TRA-MBP02 on 1/9/17.
//  Copyright © 2017 TRA-MBP02. All rights reserved.
//

import UIKit

class TimeTableScrollView: UIScrollView, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var topBar: UIView?
    @IBOutlet weak var sideTblView : UITableView?
    @IBOutlet weak var centerColView : UICollectionView?
    
    var sideDataArray = ["","08:00 - 09:00","08:00 - 09:00","08:00 - 09:00","08:00 - 09:00","08:00 - 09:00","08:00 - 09:00","08:00 - 09:00"]
    var centerDataArray = ["EDU 195","","","","","","","","","","","","","","",
                           "","","","","EDU 195","","","EDU 195","","","","","","","",
                           "","","","","","","","","","EDU 195","","","","","",
                           "EDU 195","","",""]
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        setupTimeTable()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setupTimeTable(){
        
        self.sideTblView?.delegate = self
        self.sideTblView?.dataSource = self
        self.sideTblView?.layer.zPosition = 1
        
        self.centerColView?.delegate = self
        self.centerColView?.dataSource = self
        
        self.delegate = self
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    // MARK: UITableViewDelegate / DataSource
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return sideDataArray.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Side-Cell", for: indexPath) as! SideTableViewCell
        
        cell.lblTime?.text = sideDataArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(GlobalStaticMethods.isPad())
        {
            return 93.5
        }
        else
        {
            return 50.5
        }
    }
    
    // MARK: UICollectionViewDelegate / DataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.centerDataArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = CenterCollectionViewCell()
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Center-Cell", for: indexPath as IndexPath) as! CenterCollectionViewCell
        cell.lblClass?.text = centerDataArray[indexPath.row]
        
        if(centerDataArray[indexPath.row] != "")
        {
            cell.btnDetail?.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
        }
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        return cell
    }
    
    
    func btnClicked(sender: UIButton)
    {
        //write the task you want to perform on buttons click event..
        print("item at \(sender.tag)")
        navigation.goToViewController(viewControllerIdentifier: VC_CLASS_DETAIL, animation: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        var reusableView : UICollectionReusableView = UICollectionReusableView()
        if(kind == UICollectionElementKindSectionHeader)
        {
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "TT-Header", for: indexPath)
        }
        
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if(GlobalStaticMethods.isPad()){
            return CGSize(width: 93.5, height: 93.0)
        }
        else{
            return CGSize(width: 49, height: 50)
        }
        
    }
    
    // MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        self.sideTblView?.layer.zPosition = 1
        
        
        self.sideTblView?.frame = CGRect(x: max(0 , self.contentOffset.x), y: (self.sideTblView?.frame.origin.y)!, width: (self.sideTblView?.frame.size.width)!, height: (self.sideTblView?.frame.size.height)!)
    }

}
