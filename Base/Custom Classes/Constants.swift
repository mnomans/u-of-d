
//
//  Constants.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 15/06/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//
import UIKit


struct Constants {
    
    static let publicKeyBase64ForEncryption = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3ti2g8HeVGjdcOLtr6L+KWcR4sz/im28eQkWlWOrOOedQdV7an7L5AqAEm5G4Srbr7suniMpV7vJCtnCXFa6g6fRTP6TisEwJnbkfvDGaWD+ldv22OBlLayY/22r+aJXsz8YEYND9hkSbU075UOg/VHHI+XdKawzGe6ke4zqJ6wIDAQAB"
    
    static let tagPublicKey  =  "ae.maf.cobu.Sample_Public"
    
    //static let baseURL  = "http://maf3.trafficdemos.net/api/app/v1/"
    
    static let baseURL    = "https://eservices.uod.edu.sa/eportal/api/ServiceAjax.svc/"
    
    static let baseURLClient = "http://roche-redev-phase2.trafficdemos.net/app_dev.php/api/app/v1/"
    
    
    static let baseURLDemo = "http://roche-redev-phase2.trafficdemos.net/app_dev.php/api/app/v1/"
    static let baseURLLocal = "http://roche-re2.zeeshan.tphp.net/app_dev.php/api/app/v1/"
    static let baseURLLocalNaeemBhai = "http://rocheapp-1.qa.tphp.net/app_dev.php/api/app/v1/"
    static let universalBaseURL = Constants.baseURLDemo
    
    
    //REQUEST URLS
    static let URL_CLEARANCE_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/mobile.aspx?action=AOtuWRnRq8uMrlVc64SUpfYcb5OgO3KHptePdH4mhbWDyvaFp1YIBc55o8brQK7Y&"
    
    static let URL_BOOKING_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8u5c5zj9ddrWnaWNMthiOXla7QpOoMBf596Ij01H+hJzg==&"
    
    static let URL_DEPUTATION_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8sBxtYifC7PhLEQ9pBO9w1vTZ7oDII36w7EK+j/NxazLw==&"
    
    static let URL_LEAVE_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8tSjrUsJ+HALLZH2qEqOGACoxRijE18R5lEmv3uZcq7cA==&"
    
    static let URL_ENGAGEMENT_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8vVXVDkSod0AHQU/QWma8nJBnnIXbAIgHGDx7Zv0RUGYw4UEpmZtS24S+6P4S0tX2Q=&"
    
    static let URL_SOFTWARE_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8tfmyXsXVGjUhqVAggHnbzRAPBo9UPwY3GgL2+uQGX4I4mhLU5AONsY&"
    
    static let URL_HARDWARE_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8v0oxSZ46tHhsyPrVIhVHNigKK66F9GmRpEppGp3aVvoiBW0awIMiRpfX8fxUZ/rrkXc7LyVCWanw==&"
    
    static let URL_MAINTENANCE_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8t4ijBc+pSMfIQMLqqn0DVWBuUTOagUgScnXU7rKRRfxyqEQRhRNwlk&"
    
    static let URL_GENERAL_REQUEST = "https://bpm.uod.edu.sa/Login.aspx?ReturnUrl=/Mobile.aspx?action=AOtuWRnRq8voreGfSCUqPTTSm30tbnoInUhrBgVbyRvZu/Ykj1X9cTpHQ6+CfiXyj1Kem9iyIIY=&"
    
    
    
    static let dotNetURL  = "http://mafconnect.mvc.trafficdemos.com/api/home/"
    
    static let MESSAGE = "Message"
    static let NOTIFICATION = "Notification"
    
    static let AUTO_LOGIN = "autoLogin"
    
    static let BOOKMARK_MSG = "This course added to your bookmarks"
    static let UNBOOKMARK_MSG = "This course removed from your bookmarks"

    static let ONLINE_COURSE = "Online Course"
    static let INCLASS_COURSE = "In-Class Course"
    
    
    static let ONLINE_COURSE_IMAGE = "onlineCourse"
    static let INCLASS_COURSE_IMAGE = "InlineCourse"
    
    static let COLOR_CELL_ALTERNATE = "#F0EADA"
    static let COLOR_CELL           = "#FFFFFF"
    static let COLOR_MAROON         = "#993D5A"
    static let COLOR_YELLOW         = "#F0EADA"
    static let COLOR_TOP_BAR        = "#003366"
    
    // Image Names
    static let RIGHT_BUTTON_IMAGE = "logout"
    static let LEFT_BUTTON_IMAGE = "gen-arrow-back"
    static let MENU_BUTTON_IMAGE = "gen-menu"
    
    //SERVICES CONSTANTS
    static let METHOD_POST = "POST"
    static let METHOD_GET  = "GET"
    
    static let KEY_GEN_USERNAME = "UserName"
    static let KEY_GEN_PASSWORD = "PassWord"
    
    static let GEN_USERNAME = "myud"
    static let GEN_PASSWORD = "my@ud#13$"
    
    static let COUNT_UNLIMITED = "10"
    
    
    //SOCIAL NETWORK URLS
    static let URL_FACEBOOK = "https://www.facebook.com/University-of-Dammam-%D8%AC%D8%A7%D9%85%D8%B9%D8%A9-%D8%A7%D9%84%D8%AF%D9%85%D8%A7%D9%85-130581383636726/"
    static let URL_TWITTER = "https://twitter.com/UOD_EDU_SA"
    static let URL_YOUTUBE = "https://www.youtube.com/user/dammamuniversity"
    
    
    //SERVICE URLs
    static let URL_LDAP = "https://appservices.uod.edu.sa/ldap.asmx"
    static let URL_SIS = "https://appservices.uod.edu.sa/sis.asmx"
    static let URL_FEATURE = "https://appservices.uod.edu.sa/features.asmx"
    
    
    
    //SERVICE NAMES
    static let SERVICE_EVENTS               = "Events"
    static let SERVICE_FEEDS                = "Feeds"
    static let SERVICE_PRES_MSG             = "PresidentMessage"
    static let SERVICE_COURSE_DATA          = "course_getData"
    static let SERVICE_RECTOR_MSG           = "PresidentMessage"
    static let SERVICE_HISTORY              = "AboutUs"
    static let SERVICE_EMP_SEARCH           = "IPTele"
    static let SERVICE_CONTACT_INFO         = "Contacts"
    static let SERVICE_VALID_USER           = "UserValid"
    static let SERVICE_GET_USER             = "GetUser"
 
    
    
    
    
    // EMPLOYEE SERVICE BASE URL
    static let BASE_URL      = "https://eservices.uod.edu.sa/eportal/api/ServiceAjax.svc/OvertimeInquiry"
    
    
    
    // EMPLOYEE SERVICE NAMES

    static let SERVICE_OVERTIME             = "OvertimeInquiry"
    static let SERVICE_COURSEINQUIRY        = "CourseInquiry"
    static let SERVICE_DEPUTATIONINQUIRY    = "DeputationInquiry"
    static let SERVICE_VICATIONINQUIRY      = "VicationInquery"

    // SERVICE HEADER
    
    static let headerArray = ["Content-Type":"application/json"]
    
}


