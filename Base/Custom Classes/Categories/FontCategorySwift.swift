//
//  FontCategorySwift.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit
extension UIFont{
    
    
    func setGeneralFontSizeUsingRatio(nameFont : String , psdFontSize : CGFloat) ->UIFont{
        
        var ratio : CGFloat
        
        //iphone
        var fontNameChanged = nameFont
        
        if fontNameChanged == "Roboto-Regular"{
            fontNameChanged = "Roboto-Regular"
        }
        
        if fontNameChanged == "Roboto-Medium"{
            fontNameChanged = "Roboto-Bold"
        }

        if GlobalStaticMethods.isPhone() {
            ratio = psdFontSize/1242

        }
        else{
            print(UIScreen.main.nativeBounds.size.height)
            print(constants.deviceType.SCREEN_HEIGHT)
            if constants.deviceType.SCREEN_HEIGHT > 1024 {
                ratio = psdFontSize/2048

            }
            else{
            ratio = psdFontSize/1536
            }
        }
        //ipad pro
        
        //ipad
        
        
        var screenSize : CGFloat
        
        let screenRect : CGRect = UIScreen.main.bounds
        
        let screenWidth: CGFloat = screenRect.size.width
        
        let screenHeight: CGFloat = screenRect.size.height
        
        
        if screenWidth<screenHeight {
            screenSize = screenWidth;
        }
        else{
            screenSize = screenHeight;
            
        }
        var fontsize = screenSize * ratio
        if fontsize < 12{
            fontsize = 12
        }
        
     //   fontNameChanged = "Roboto"
        
        return UIFont(name: fontNameChanged, size: fontsize)!
        
        
        
    }
}
