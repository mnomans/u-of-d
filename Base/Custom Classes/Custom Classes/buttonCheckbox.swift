//
//  buttonCheckbox.swift
//  Gemini-designsOnly
//
//  Created by Ahsan Ebrahim Khatri on 8/2/16.
//  Copyright © 2016 Traffic. All rights reserved.
//

import UIKit
@IBDesignable
class buttonCheckbox: UIButton {
    
    @IBInspectable var isButtonSelected : Bool = false{
        didSet{
            updateImage()
        }
    }
    var selectedState : Bool!
    
    @IBInspectable var selectedImage : String!
    @IBInspectable var unselectedImage : String!
    var selectedImageForAll : UIImage!
    var unselectedImageForAll : UIImage!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(buttonCheckbox.buttonTapped), for: .touchUpInside)
        self.selectedState = isButtonSelected
        selectedImageForAll = UIImage (setImageForPro: selectedImage)
        unselectedImageForAll = UIImage (setImageForPro: unselectedImage)
        updateImage()
        
    }
    
    func buttonTapped(){
//        if self.isButtonSelected == true{
//           // self.setBackgroundImage(unselectedImage, forState: UIControlState .Normal)
//            isButtonSelected    = false
//        }
//        else 
        if self.isButtonSelected == false{
           // self.setBackgroundImage(selectedImage, forState: UIControlState .Selected)
            isButtonSelected = true
        }
        updateImage()
    }
    
    func updateImage(){
        if (isButtonSelected){
            self.setBackgroundImage(selectedImageForAll, for: UIControlState .normal)
        }else{
            self.setBackgroundImage(unselectedImageForAll, for: UIControlState .normal)
        }
    }

}
