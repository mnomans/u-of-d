 //
//  BaseUITextField.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

class BaseUITextField: UITextField {
    @IBInspectable var underLineTextfield : Bool = false
    @IBInspectable var customizeFont: Bool = false
    
    
    
    
    var textFieldType : Int?
    
    
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    //    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = ""
    @IBInspectable var bottomBorderHeight : CGFloat = 1
    @IBInspectable var bottomBorderColor : String = ""
    
    @IBInspectable var ShowAcitvityIndicator :  Bool = false

    
    @IBInspectable var leftImage : String! = ""{
        didSet{
            
            
            setImage()
        }
    }
    @IBInspectable var rightImage : String! = ""{
        didSet{
            setImage()
        }
    }
    
    var bottomBorder : UIView!
    let defaultFontVal: CGFloat = 50.0
    
    var imageView = UIImageView.init()
    
    var activityIndicator = UIActivityIndicatorView.init()

//    override func drawRect(rect: CGRect) {
//       
//
//    }
    
 

 
    override func awakeFromNib() {
        
       
   //      self.setImage()
        
        if customizeFont == true {
            self .customizeFontSizes()

        }

        
       
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
   
        if underLineTextfield == true{
            
            self.underlineUpdatedMethod()
        }
        
       // if rightImage != "" || leftImage != "" {
       // }
      
    }
    
    
    func customizeFontSizes(){
        
        let myfont = self.font
        
       
        if GlobalStaticMethods.isPhone(){
            self.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:iPhoneFontSize )
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:iPadProFontSize )
                
            }
            else {
                self.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:iPadFontSize )
                
            }
            
        }

        
    }
    
    
    func underlineUpdatedMethod(){
        
        if bottomBorder == nil {
            if bottomBorderColor == "" {
                bottomBorderColor = "#FFFFFF"
            }
            if bottomBorderHeight < 1 {
                bottomBorderHeight = 1
            }
            
            
             bottomBorder =  UIView(frame: CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: bottomBorderHeight))
            bottomBorder.backgroundColor = UIColor(hexString: bottomBorderColor )
            
            self.addSubview(bottomBorder)
        }
        
        bottomBorder.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: bottomBorderHeight)
        
      
       
    }
    func createUnderLine(){
        
        self.borderStyle = UITextBorderStyle.none

        var deviceVal : CGFloat = 2
        if constants.deviceType.IS_IPHONE_5{
            deviceVal = 10
        }
        else if constants.deviceType.IS_IPHONE_6{
            deviceVal = 4
        }
        else if constants.deviceType.IS_IPHONE_6P{
            deviceVal = 2
        }
        
        
        let bottomLine = CALayer()
        let yPosition = self.frame.height - deviceVal
        bottomLine.frame = CGRect(x: 0.0, y: yPosition, width: self.frame.width, height: 1)
        
        bottomLine.backgroundColor = UIColor.white.cgColor
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0.0
        self.layer.addSublayer(bottomLine)
        self.layer.masksToBounds = true // the most important line of code

    }
    
    func setImage()
    {

        
        if leftImage != ""
        {
            var bgView = UIView.init()
            
            if GlobalStaticMethods.isPad()
            {
                bgView = UIView.init(frame: CGRect(x: 0 , y: 0 , width:  self.frame.width * 0.05, height : self.frame.height))
            }
            else
            {
                 bgView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.frame.width * 0.075, height: self.frame.height )) // CGRectMake(0, 0, self.frame.width * 0.075, self.frame.height))
            }
            
         
            
            imageView = UIImageView(image: UIImage.init(setImageForPro: leftImage))
            imageView.contentMode = UIViewContentMode.left
            
            imageView.frame = CGRect(x : 0, y : (self.frame.height -  self.frame.height*0.80)/2, width :  self.frame.width*0.05, height : self.frame.height*0.80)
            
            imageView.contentMode = .center

            bgView .addSubview(imageView)
            
            imageView.center = bgView.center;
     
            self.leftView = bgView

            self.leftViewMode = UITextFieldViewMode.always
            
        }else{
            self.leftView?.subviews.forEach { $0.removeFromSuperview() }
        }
        if ShowAcitvityIndicator
        {
            var bgView = UIView.init()
            if GlobalStaticMethods.isPad()
            {
                bgView = UIView.init(frame: CGRect(x : 0, y :  0, width : self.frame.width * 0.05, height : self.frame.height))
            }
            else
            {
                bgView = UIView.init(frame: CGRect(x : 0, y : 0, width :  self.frame.width * 0.075, height :self.frame.height))
            }
            activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
            
            bgView .addSubview(activityIndicator)
            
            activityIndicator.center = bgView.center;
            activityIndicator.startAnimating()
            
            self.rightView = bgView
            self.rightViewMode = UITextFieldViewMode.always
        }
        else if (rightImage != nil) && (!ShowAcitvityIndicator) && rightImage != ""
        {
            var bgView = UIView.init()
            if GlobalStaticMethods.isPad()
            {
                bgView = UIView.init(frame: CGRect(x: 0, y : 0, width : self.frame.width * 0.05, height :  self.frame.height))
            }
            else
            {
                bgView = UIView.init(frame: CGRect(x : 0, y: 0,width :  self.frame.width * 0.075, height : self.frame.height))
            }

            
            
                   imageView = UIImageView(image: UIImage(setImageForPro: rightImage))
            
            
            imageView.contentMode = UIViewContentMode.right
            imageView.frame = CGRect(x : self.frame.width * 0.11-self.frame.width*0.05, y : (self.frame.height -  self.frame.height*0.80)/2, width : self.frame.width*0.04, height : self.frame.height*0.80)
            imageView.contentMode = .center

             bgView .addSubview(imageView)
            
            imageView.center = bgView.center;

            self.rightView = bgView
            self.rightViewMode = UITextFieldViewMode.always
        }else{
            self.rightView?.subviews.forEach { $0.removeFromSuperview() }
        }
        
        self.layoutIfNeeded()
    }
    
    
      //  self.borderStyle = .None
//        self.backgroundColor = UIColor.clearColor()
//        let border = CALayer()
//        let width = CGFloat(2.0)
//        border.borderColor = UIColor.darkGrayColor().CGColor
//       // border.borderWidth = 3.0
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
//        
//        border.borderWidth = width
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
    
    
    

}
