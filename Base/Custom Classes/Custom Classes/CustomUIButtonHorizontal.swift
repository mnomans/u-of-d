//
//  CustomUIButtonHorizontal.swift
//  Buttons
//
//  Created by Waqas Ali on 01/08/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import UIKit

public class CustomUIButtonHorizontal: BaseUIButton {
    
    // MARK: Public interface
  
     @IBInspectable var titleLeftImageRightCorner : Bool = false
     @IBInspectable var onlyImage : Bool = false
    
    
    
    
    // MARK: Overrides
    
    
    
     public override func awakeFromNib() {
        super.awakeFromNib()
        
        self .setTitleAndImageCorners(isInverse: titleLeftImageRightCorner)
        
    }
    
    
     public override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
    }
    
    func setTitleAndImageCorners(isInverse : Bool){
        
     
       
  
   
        if(isInverse == true){
            
            if(onlyImage == false){
            let spacingTitle =  CGFloat ( self.frame.size.width - (self.imageView!.frame.size.width + self.titleLabel!.frame.size.width + self.frame.height * 0.5)) + self.frame.height * 0.5
            self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacingTitle )
            }
            let spacingImage = CGFloat ( self.frame.size.width  - (self.imageView!.frame.size.width + self.frame.height * 0.5 )) - UIView.convertToRatio(value: 6)
                self.imageEdgeInsets = UIEdgeInsetsMake(0, spacingImage, 0, 0);
        }else{
            
             if(onlyImage == false){
            let spacingTitle =  CGFloat ( self.frame.size.width - (self.imageView!.frame.size.width + self.titleLabel!.frame.size.width +  self.frame.height * 0.5))  - self.frame.height * 0.5
              self.titleEdgeInsets = UIEdgeInsetsMake(0, spacingTitle, 0, 0 )
            }
            
            let spacingImage = CGFloat ( self.frame.size.width - (self.imageView!.frame.size.width + self.titleLabel!.frame.size.width + self.frame.height * 0.5) )  - self.frame.height * 0.5
            
              self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacingImage);
                
                
            
            
           
        }
        
   
        
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        

        
        
        
        
    }
    
    
    
}
