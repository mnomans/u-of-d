//
//  buttonFontCustomClass.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

//@IBDesignable
class buttonFontCustomClass: UIButton {
    
    
    @IBInspectable var mafConnectbutton : Bool = false
    
    
    let defaultFontVal: CGFloat = 50.0
    let defaultBorderVal: CGFloat = 2.0
    @IBInspectable var fontColor : String = "#FFFFFF"{
        didSet{
            updateColor()
        }
    }
    
    
    @IBInspectable var fontChanges : Bool = false
    @IBInspectable var spacingImageText : Bool = false
    
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = "MarselisPro"
    @IBInspectable var borderWidth : CGFloat = 2
    @IBInspectable var borderColor : String = "#FFFFFF"{
        didSet{
            updateColor()
        }
    }
    @IBInspectable var highlightedColor : String = "#FFFFFF"
    @IBInspectable var highlightedFontColor : String = "#FFFFFF"
    
    @IBInspectable var unhighlightedColor : String = "#FFFFFF"
    @IBInspectable var highlightImage : String?
    
    /*
     
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        
        if mafConnectbutton == true{
            self.buttonForMAFConnect()
        }
        
    }
    override func awakeFromNib() {
        
        self.addTarget(self, action: #selector(buttonFontCustomClass.highlightButton), for: .touchDown)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), for: .touchUpInside)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), for: .touchDragOutside)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), for: .touchCancel)
        self.backgroundColor = UIColor(hexString: unhighlightedColor)
        
        self .imagePro()
        if highlightImage != nil {
            
            let str = "\(highlightImage!)"
            
            let image = UIImage(setImageForPro: highlightImage!)
            
            self.setImage(image, for: .highlighted)
            
        }
        
        
        
        //        if mafConnectbutton == true{
        //            self.buttonForMAFConnect()
        //        }
        
        if fontChanges == true{
            self .customizeFontSizes()
        }
        
        
        
        
        
        if spacingImageText == true{
            var spaceX : CGFloat = 30
            if constants.deviceType.IS_IPHONE_5{
                spaceX = 20
            }
            else if constants.deviceType.IS_IPHONE_6{
                spaceX = 25
            }
            else if constants.deviceType.IS_IPHONE_6P{
                spaceX = 30
            }
            self.centerButtonTitleAndImage(spacing: spaceX)
        }
    }
    
    
    func highlightButton(){
        if highlightedColor == "cc"{
            self.backgroundColor = UIColor.clear
            
            
        }
        else{
            self.backgroundColor = UIColor(hexString: highlightedColor)
            self.setTitleColor(UIColor(hexString: highlightedFontColor ), for: .highlighted)
            
            
        }
        
    }
    func unhighlightButton(){
        if unhighlightedColor == "cc"{
            self.backgroundColor = UIColor.clear
            
        }
        else{
            self.backgroundColor = UIColor(hexString: unhighlightedColor)
            self.setTitleColor(UIColor(hexString: fontColor ), for: .normal)
            
            
        }
        //        if highlightImage != nil {
        //            self.setImage(UIImage(imageName: highlightImage!), forState: .Normal)
        //
        //        }
        
    }
    func customizeFontSizes(){
        
        let myfont = self.titleLabel?.font
        
        self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:self.iPhoneFontSize )
        
        
        
        
        if GlobalStaticMethods.isPhone(){
            self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:self.iPhoneFontSize )
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:self.iPadProFontSize )
                
            }
            else {
                self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:self.iPadFontSize )
                
            }
            
        }
        
        
        
    }
    
    func imagePro(){
        
        
        if imageForIpadPro != nil {
            
            // let str = "\(imageForIpadPro!)-sel"
            
            let image = UIImage(setImageForPro: imageForIpadPro!)
            
            self.setImage(image, for: .normal)
            
            
        }
        
        //        guard self.imageForIpadPro.characters.count < 0 || self.imageForIpadPro == "" else{
        //            let myImagePro = self.imageView?.image
        //
        //            return   self .setImage(myImagePro, forState: .Normal)
        //
        //        }
    }
    
    
    func buttonForMAFConnect(){
        
        
        self.layer.cornerRadius = frame.size.height * 0.5 //0.06038 * UIScreen.mainScreen().bounds.size.width;
        self.layer.borderWidth = borderWidth
        //  self.layer.backgroundColor = UIColor.clearColor().CGColor
        //--ww self.titleLabel?.textColor = UIColor(hexString: borderColor)
        updateColor()
        //
        //self.setTitleAndImageAtCorner(20)
        
        
    }
    
    func updateColor(){
        
        self.setTitleColor(UIColor(hexString: fontColor), for: .normal)
        
        self.layer.borderColor = UIColor(hexString: borderColor ).cgColor
        
    }
}
