//
//  CustomUISlider.swift
//  Gemini
//
//  Created by Ahsan Ebrahim Khatri on 9/15/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class CustomUISlider: UISlider {
    
    @IBInspectable var iPhoneHeight : CGFloat = 3.3
    @IBInspectable var iPadHeight : CGFloat = 4.5
    @IBInspectable var iPadProHeight : CGFloat = 6.0

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newBounds = super.trackRect(forBounds: bounds)
        if GlobalStaticMethods.isPhone(){
            newBounds.size.height = iPhoneHeight
        }else if GlobalStaticMethods.isPadPro(){
            newBounds.size.height = iPadProHeight
        }else{
            newBounds.size.height = iPadHeight
        }
        return newBounds
    }

}
