//
//  BaseLayoutConstraint.swift
//  Buttons
//
//  Created by Waqas Ali on 01/08/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import UIKit

public class BaseLayoutConstraint: NSLayoutConstraint {
   
    override public func awakeFromNib() {
        super.awakeFromNib();
        //--
        
        self.constant =  UIView.getValueFromRatio(value: constant)
        
       
    } //F.E.
} //CLS END
