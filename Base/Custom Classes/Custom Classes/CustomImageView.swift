//
//  CustomImageView.swift
//  Skeleton
//
//  Created by  Traffic MacBook Pro on 20/06/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

//@IBDesignable
class CustomImageView: UIImageView {

    @IBInspectable var imageForAll : String?{
        didSet{
            setImage()
        }
    }
    
    
    
    override func awakeFromNib() {
        self .setImage()
    }

    
    func setImage(){
        
        
        let imageName = imageForAll ?? "placeholder"
        
        self.image = UIImage(setImageForPro: imageName)
        
        
//        if GlobalStaticMethods.isPad(){
//            if GlobalStaticMethods.isPadPro(){
//                self.image = UIImage(named: "\(imageName)-pro")
//            
//            }
//            else{
//                self.image = UIImage(named: imageName)
//            }
//        }
//        else{
//            self.image = UIImage(named: imageName)
//        }
        
        
        
    }
    
    
    
    
    
    
    
}
