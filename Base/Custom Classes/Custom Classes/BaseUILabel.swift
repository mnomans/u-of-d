  //
//  BaseUILabel.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

class BaseUILabel: UILabel {
    
    

    @IBInspectable var iPhoneFontSize : CGFloat = 50.0{
        didSet{
            customizeFontSizes()
        }
    }
    @IBInspectable var iPadFontSize : CGFloat = 50.0{
        didSet{
            customizeFontSizes()
        }
    }

    @IBInspectable var iPadProFontSize : CGFloat = 50.0{
        didSet{
            customizeFontSizes()
        }
    }

    //    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = "Roboto-Regular"
        {
        didSet{
            customizeFontSizes()
        }
    }

    let defaultFontVal : CGFloat = 50.0
    
    

    
    override func awakeFromNib() {
        self .customizeFontSizes()
    }
    
    
    
    override func layoutSubviews() {
        self.customizeFontSizes()
    }
    
    func setAttributedStringWithHtml(htmlStr : String)
    {
        var fontSize : CGFloat
        if GlobalStaticMethods.isPhone(){
            fontSize = fontClass.getFontSize(psdFontSize: iPhoneFontSize)
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                fontSize = fontClass.getFontSize(psdFontSize: iPadProFontSize)
            }
            else {
                fontSize = fontClass.getFontSize(psdFontSize: iPadFontSize)
            }
        }
        
        self.attributedText = stringsClass.getAttributedStringForHTMLWithFont(htmlStr: htmlStr, textSize: Int(fontSize), fontName: fontName)
    }
    
    func customizeFontSizes(){
//        if iPhoneFontSize <= 33{
//            iPhoneFontSize = 25
//        }
//        if iPadFontSize < 35{
//            iPadFontSize = 36
//        }
//        if iPadProFontSize < 35{
//            iPadProFontSize = 36
//        }
        let myfont = self.font
        
        if GlobalStaticMethods.isPhone(){
            
            self.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName, psdFontSize:iPhoneFontSize )
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName, psdFontSize:iPadProFontSize )
                
            }
            else {
                self.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName, psdFontSize:iPadFontSize )
                
            }
            
        }
        
        
        
    }
    
}
