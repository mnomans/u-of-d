//
//  BaseUITextView.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

class BaseUITextView: UITextView {
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    //    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = "Roboto-Regular"
    let defaultFontVal: CGFloat = 50.0
//    @IBInspectable var minFontSizePhone : CGFloat = 44.0
//    @IBInspectable var minFontSizePad : CGFloat = 44.0
    
    override func awakeFromNib() {
        self .customizeFontSizes()
    }
    
    func setAttributedStringWithHtml(htmlStr : String)
    {
        var fontSize : CGFloat
        if GlobalStaticMethods.isPhone(){
           fontSize = fontClass.getFontSize(psdFontSize: iPhoneFontSize)
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                fontSize = fontClass.getFontSize(psdFontSize: iPadProFontSize)
            }
            else {
                fontSize = fontClass.getFontSize(psdFontSize: iPadFontSize)
            }
        }
        
        self.attributedText = stringsClass.getAttributedStringForHTMLWithFont(htmlStr: htmlStr, textSize: Int(fontSize), fontName: fontName)
    }
    
    func customizeFontSizes(){
        //  self.font = UIFont(name: "Roboto-Regular", size: iPhoneFontSize)!
        
       // self.font = UIFont(name: "Roboto-Regular")


        let myfont  = UIFont(name: "Roboto-Regular", size: iPhoneFontSize)!
        
        if(self.font != nil)
        {
            
            
            self.font = myfont.setGeneralFontSizeUsingRatio(nameFont: fontName, psdFontSize: iPhoneFontSize)
        }
        
//        if self.iPhoneFontSize < minFontSizePhone{
//            self.iPhoneFontSize = minFontSizePhone
//        }
//        if self.iPadFontSize < minFontSizePad {
//            self.iPadFontSize = minFontSizePad
//        }
        
        if GlobalStaticMethods.isPhone(){
            
            self.font = myfont.setGeneralFontSizeUsingRatio(nameFont: fontName, psdFontSize: iPhoneFontSize)

            

            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.font = myfont.setGeneralFontSizeUsingRatio(nameFont: fontName, psdFontSize: iPadProFontSize)


            }
            else {
              //  self.font = UIFont(name: "Roboto-Regular", size: 20)!
                self.font = myfont.setGeneralFontSizeUsingRatio(nameFont: fontName, psdFontSize: iPadFontSize)

            }
            
        }
        
        
    }
    

}
