//
//  CustomButton.swift
//  Buttons
//
//  Created by Waqas Ali on 01/08/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//


import UIKit


public class BaseUIButton: UIButton {
    
    // MARK: Public interface
    @IBInspectable var isRounded : Bool = false
    @IBInspectable var cornerRadius : CGFloat = 0
    @IBInspectable var imageRightTitleLeft : Bool = false
    
    @IBInspectable var borderWidth : CGFloat = 2
    @IBInspectable var borderColor : String = "#000000"
    @IBInspectable var highlightedColor : String = "cc"
    @IBInspectable var unhighlightedColor : String = "cc"
    @IBInspectable var revertFontColorOnTap : Bool = false
    @IBInspectable var fontHighlightedColor : String = "cc"
    @IBInspectable var fontUnHighlightedColor : String = "cc"
    @IBInspectable var spacingImageText : CGFloat = 5.0
    
    @IBInspectable var customizeFont: Bool = false
    
    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 45.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    @IBInspectable var fontName : String = "MarselisPro"
    let defaultFontVal: CGFloat = 50.0
    
    
    
    // MARK: Overrides
    
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.isExclusiveTouch = true
        
        if(isRounded == true){
            layoutRoundBorderLayer()
        }
        
        
        
        self.addTarget(self, action: #selector(buttonFontCustomClass.highlightButton), for: .touchDown)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), for: .touchUpInside)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), for: .touchDragOutside)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), for: .touchCancel)
        
        self.unhighlightButton()
        
        
        
        self.titleLabel?.font = UIFont(name: self.titleLabel!.font.fontName, size: UIView.convertFontSizeToRatio(value: self.titleLabel!.font.pointSize, fontStyle:nil , sizedForIPad:true));
        
        if customizeFont == true {
            self .customizeFontSizes()
        }
        
        self.setImageForIpadPro()
        self .setTitleAndImageSpacing(spacing: spacingImageText)
        
        
    }
    
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    // MARK: Private
    
    private func layoutRoundBorderLayer() {
        
        
        
        let layer:CALayer = self.layer
        layer.borderColor = UIColor(hexString: borderColor ).cgColor
        layer.cornerRadius = self.frame.size.height * 0.5
        if(cornerRadius > 0){
            layer.cornerRadius = cornerRadius
        }
        layer.masksToBounds = true
        layer.borderWidth = 2
    }
    
    func highlightButton(){
        if highlightedColor == "cc"{
            self.backgroundColor = UIColor.clear
            
        }
        else{
            self.backgroundColor = UIColor(hexString: highlightedColor)
            
            if(revertFontColorOnTap == true ){
                
                self.setTitleColor(UIColor(hexString: unhighlightedColor), for: .normal)
            }else{
                if(fontHighlightedColor != "cc"){
                    self.setTitleColor(UIColor(hexString: fontHighlightedColor), for: .normal)
                    
                }
                
                
            }
            
        }
        
    }
    func unhighlightButton(){
        
        
        if unhighlightedColor == "cc"{
            self.backgroundColor = UIColor.clear
            
        }
        else{
            self.backgroundColor = UIColor(hexString: unhighlightedColor)
            if(revertFontColorOnTap == true){
                
                self.setTitleColor(UIColor(hexString: highlightedColor), for: .normal)
            }else{
                if(fontUnHighlightedColor != "cc"){
                    self.setTitleColor(UIColor(hexString: fontUnHighlightedColor), for: .normal)
                    
                }else{
                    
                    self.setTitleColor(UIColor(hexString: "#FFFFFF"), for: .normal)
                    
                }
                
                
            }
        }
    }
    
    
    
    func setImageForIpadPro(){
        
        
        if imageForIpadPro != nil {
            
            // let str = "\(imageForIpadPro!)-sel"
            
//            self.setImage(UIImage(named: imageForIpadPro!), forState: .Normal)
            let image = UIImage (setImageForPro: imageForIpadPro!)
            self.setImage(image, for: .normal)
            
            if(self.imageView?.highlightedImage != nil){
                
                let str = "\(imageForIpadPro!)-sel"
                
                self.setImage(UIImage(named: str), for:.highlighted)
            }
            
        }
    }
    
    
    func setTitleAndImageSpacing(spacing : CGFloat){
        
        
        let insetAmount :CGFloat = UIView.convertToRatio(value: spacing)
        
        // UIEdgeInsetsMake(<#T##top: CGFloat##CGFloat#>, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
        
        if(imageRightTitleLeft == true){
            
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -(self.imageView!.frame.size.width + insetAmount), 0, self.imageView!.frame.size.width);
            self.imageEdgeInsets = UIEdgeInsetsMake(0, self.titleLabel!.frame.size.width, 0, -(self.titleLabel!.frame.size.width + insetAmount));
            
        }else{
            
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, -insetAmount)
            
            self.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
        }
        self.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, insetAmount);
        
    }
    
    
    
    
    
    func customizeFontSizes(){
        
        let myfont = self.titleLabel!.font
        
        
        if GlobalStaticMethods.isPhone(){
            self.titleLabel!.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:iPhoneFontSize )
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.titleLabel!.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:iPadProFontSize )
                
            }
            else {
                self.titleLabel!.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: self.fontName , psdFontSize:iPadFontSize )
                
            }
            
        }
        
        
    }
    
}
