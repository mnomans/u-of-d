//
//  kevTutorialView.swift
//  Test
//
//  Created by  Traffic MacBook Pro on 25/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class kevTutorialView: UIView,UIScrollViewDelegate {
    
    
    let kevScrollView = UIScrollView()
    
    let kevPageControl = UIPageControl()
    
    let kevSkipButton  = UIButton()
    
    var viewsArray : NSMutableArray! = []
    
    var viewsDictionary = Dictionary<String,AnyObject>()
    
    var mainViewsContraints = [NSLayoutConstraint]()
    
    
    var sliderImageViewContraints = Array<[[NSLayoutConstraint]]>()
    
    var arrTutorialImage : Array<String>?
        {
        didSet{
        }
    }
    
    
    var scrollWidth : CGFloat! = 0.0
        {
        didSet{
            //   tutorialImplementation()
            
        }
    }
    
    
    var scrollHeight : CGFloat! = 0.0
        {
        didSet{
            //   tutorialImplementation()
            
        }
    }
    
    
    var total : Int = 0
    
    
    var currentPage : Int = 0
    
    
    var landscapeOrient : Bool = false
    
    
    
    
    override func awakeFromNib() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(kevTutorialView.setConstraintsWithWidth), name: NSNotification.Name.UIApplicationDidChangeStatusBarOrientation, object: nil)
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(kevTutorialView.configureTutorialScreen), userInfo: nil, repeats: false)
        
        
    }
    
    func configureTutorialScreen(){
        
        
        scrollWidth = self.frame.width
        scrollHeight = self.frame.height
        
        
        //ScrollView
        kevScrollView.backgroundColor = UIColor.clear
        kevScrollView.isPagingEnabled = true
        kevScrollView.delegate = self
        kevScrollView.translatesAutoresizingMaskIntoConstraints = false
        kevScrollView.showsHorizontalScrollIndicator = false
        kevScrollView.showsVerticalScrollIndicator = false
        
        self.addSubview(kevScrollView)
        
        //PageControl
        kevPageControl.currentPage = 0
        kevPageControl.numberOfPages = arrTutorialImage!.count
        kevPageControl.backgroundColor = UIColor.clear
        kevPageControl.tintColor = UIColor.black
        kevPageControl.pageIndicatorTintColor = UIColor.gray
        kevPageControl.currentPageIndicatorTintColor = UIColor.blue
        kevPageControl.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(kevPageControl)
        
        
        //adding to views dictionary
        self.viewsDictionary["kevScroll"] = kevScrollView
        self.viewsDictionary["kevPage"] = kevPageControl
        //self.viewsDictionary["skipButton"] = kevSkipButton
        
        
        setConstraintsForMainViews()
        
    }
    
    func setConstraintsForMainViews() {
        
        // adding constraints for scrollView
        //Vertical Constraints
        let strVConstriant = "V:|-0-[kevScroll]-0-[kevPage(30)]-5-|"
        //HorizontalConstraints
        var strHConstriant = "H:|-0-[kevScroll]-0-|"
        addConstraintForMain(strVConstriant)
        addConstraintForMain(strHConstriant)
        
        //adding constranits for page control
        //vertical contraints
        //   strVConstriant = "V:[kevPage]-5-|"
        //Horizontal Constriants
        strHConstriant = "H:[kevPage(==kevScroll)]"
        addPageConstraint(strHConstriant)
        
        NSLayoutConstraint.activate(mainViewsContraints)
        
        
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(kevTutorialView.tutorialImplementation), userInfo: nil, repeats: false)
        
        //  self .performSelector(#selector(kevTutorialView.tutorialImplementation), withObject: nil, afterDelay: 1)
        //    tutorialImplementation()
    }
    
    func addConstraintForMain(_ format: String){
        
        
        let newContraint = NSLayoutConstraint.constraints(withVisualFormat: format, options: [], metrics: nil, views: self.viewsDictionary)
        
        self.mainViewsContraints += newContraint
        
    }
    
    
    func addPageConstraint(_ format:String){
        let newContraint = NSLayoutConstraint.constraints(withVisualFormat: format, options: .alignAllCenterX, metrics: nil, views: self.viewsDictionary)
        // let newConstraint = NSLayoutConstraint.constraint
        self.mainViewsContraints += newContraint
        
    }
    
    deinit{
        print("agaya bhia agaya")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidChangeStatusBarOrientation, object: nil)
        
    }
    
    func tutorialImplementation() {
        
        //  scrollWidth = CGRectGetWidth(self.frame)
        //scrollHeight = CGRectGetHeight(self.frame)
        total = arrTutorialImage!.count
        for i in 0...total - 1 {
            _ = "v\(i)"
            
            
            
            let imgView = UIImageView(image: UIImage(named: arrTutorialImage![i]))
            imgView.contentMode = .scaleAspectFit
            imgView.translatesAutoresizingMaskIntoConstraints = false
            
            //    imgView.backgroundColor = UIColor.init(red: 223/255, green: 232/255, blue: 242/255, alpha: 1.0)
            
            // imgView.transform = CGAffineTransformMakeScale(0.75, 0.75)// test remove when replace with orginal
            
            self.kevScrollView.addSubview(imgView)
            
            
            
            
            //            if i % 2 == 0 {
            //                imgView.backgroundColor = UIColor.blackColor()
            //
            //            }
            //            else{
            //                imgView.backgroundColor = UIColor.grayColor()
            //
            //            }
            
            let str = "imgView\(i)"
            //let str = "imgView"
            //       let dictImgView = Dictionary(dictionaryLiteral: (str, imgView ))
            
            viewsDictionary[str]  = imgView
            let scrollwidth = self.kevScrollView.frame.width
            let scrollHeight = self.kevScrollView.frame.height
            
            
            let red_constraint_H = NSLayoutConstraint.constraints(withVisualFormat: "H:[imgView\(i)(\(scrollwidth))]", options: [], metrics: nil, views: viewsDictionary)
            
            red_constraint_H.first?.identifier = "first constraint masla H"
            
            let red_constraint_V = NSLayoutConstraint.constraints(withVisualFormat: "V:[imgView\(i)(\(scrollHeight))]", options: [], metrics: nil, views: viewsDictionary)
            
            red_constraint_V.last?.identifier = "first constraint masla V"
            
            viewsArray!.add(imgView)
            
            
            //  viewsDictionary[key] = viewsArray![i]
            
            
            // Get Constraint into array
            
            
            let arr = [red_constraint_V,red_constraint_H]
            
            sliderImageViewContraints .append(arr)
            imgView .addConstraints(red_constraint_H)
            imgView.addConstraints(red_constraint_V)
            
            
        }
        
        
        setVerticalAndHorizontalSpacingOfImagesFromSlider()
        
        kevScrollView.contentSize = CGSize( width: CGFloat(total) * scrollWidth!, height: CGFloat(total) * scrollHeight!)
        
        
    }
    
    
    
    
    func setVerticalAndHorizontalSpacingOfImagesFromSlider(){
        
        var strContraintSet = NSMutableString.init(string: "H:|-0-")
        
        
        var dictView : Dictionary<String,AnyObject> = Dictionary()
        
        
        
        for i in 0...viewsArray!.count - 1 {
            
            let key = "v\(i)"
            
            
            dictView[key] = "\(viewsArray![i])" as AnyObject?
            
            dictView .updateValue( (viewsArray![i] as AnyObject), forKey: key)
            
            strContraintSet = NSMutableString(string: "\(strContraintSet)[\(key)]-0-")
            
            if i == viewsArray!.count - 1{
                
                strContraintSet = NSMutableString(string: "\(strContraintSet)|")
                
            }
            
            
        }
        
        
        let metrics: [String : AnyObject] = ["vSpacing": 0 as AnyObject, "hSpacing": 0 as AnyObject]
        self .layoutIfNeeded()
        
        for i in 0...viewsArray!.count - 1 {
            
            
            let constraints_POS_V = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v\(i)]", options: [], metrics: nil, views: dictView)
            
            
            
            // let constraints_POS_V = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[v0]-|", options: [] , metrics: nil, views: dictView  )
            
            
            
            self.addConstraints(constraints_POS_V)
            
        }
        
        
        
        self .updateConstraints()
        self .layoutIfNeeded()
        
        //   let constraint_POS_ = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-", options: [], metrics: nil, views: dictView)
        
        let constraint_POS_H = NSLayoutConstraint.constraints(withVisualFormat: strContraintSet as String, options: [], metrics: metrics, views: dictView)
        
        self.addConstraints(constraint_POS_H)
        
        
        
    }
    
    
    func setConstraintsWithWidth(){
        
        if UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
            
            if  isPadPro() {
                self.scrollWidth = self.kevScrollView.frame.width
                self.scrollHeight = self.kevScrollView.frame.height
                
            }
            else  if isPad(){
                
                self.scrollWidth = self.kevScrollView.frame.width
                self.scrollHeight = self.kevScrollView.frame.height
            }
            else {
                self.scrollWidth = 736
                self.scrollHeight = 414 - 25
            }
        }
        else{
            self.scrollWidth = 414
            self.scrollHeight = 736 - 25
        }
        //        if landscapeOrient == false {
        //            print("Chaboooooo")
        //        }
        //        else{
        //
        
        
        for i in 0...sliderImageViewContraints.count - 1 {
            
            let constraintArr = sliderImageViewContraints[i].first![0]
            
            
            
            
            
            let constant1 : NSLayoutConstraint = constraintArr as! NSLayoutConstraint
            
            constant1.identifier = "Masla"
            
            constant1.constant = self.scrollWidth
            
            sliderImageViewContraints[i][0] = [constant1]
            
            let constraintArr2 = sliderImageViewContraints[i].last![0]
            
            
            
            let constant2 : NSLayoutConstraint = constraintArr2 as! NSLayoutConstraint
            
            constant2.constant = self.scrollHeight
            constant2.identifier = "dosra Masla"
            sliderImageViewContraints[i][1] = [constant1]
            
            
            self.updateConstraints()
            
            
            self.layoutIfNeeded()
            
            // constant1.firstItem.constant = self.scrollWidth
            
        }
        
        gotoPage(false)
        //    }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        kevPageControl.currentPage = Int(pageNumber)
    }
    
    
    func gotoPage(_ animated: Bool){
        let width = Int( self.frame.width)
        let xVal = CGFloat(width  * currentPage)
        self .kevScrollView.scrollRectToVisible(CGRect(x: xVal, y: 0, width: self.frame.width, height: self.frame.height), animated: animated)
        
        
    }
    
    func isPhone()->Bool{
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone{
            
            return true
        }
        else
        {
            return   false
            
        }
    }
    func isPad()->Bool{
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad{
            
            return true
        }
        else
        {
            return   false
            
        }
    }
    
    
    func isPadPro()->Bool{
        
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad &&
            (UIScreen.main.bounds.size.height == 1366 || UIScreen.main.bounds.size.width == 1366)) {
            return true
        }
        return false
        
        
    }
    
    
    
}
