//
//  AlamofireRequestHelper.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import NVActivityIndicatorView



class AFWrapper: NSObject , NVActivityIndicatorViewable{
    
    static var viewsDictionary = Dictionary<String,AnyObject>()
    static var constraints = [NSLayoutConstraint]()
    
    class func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func LoadImage(strUrl : String, imgView : UIImageView, placeholder : String)
    {
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = imgView.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        imgView.addSubview(myActivityIndicator)
        
        //let imageCache = AutoPurgingImageCache()
        //let avatar = imageCache.image(withIdentifier: "avatar")
        //let circularAvatar = imageCache.image(for: urlRequest, withIdentifier: "circle")
        
        Alamofire.request(strUrl).responseImage { response in
            debugPrint(response)
            
            //print(response.request)
            //print(response.response)
            //debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                imgView.image = image
                myActivityIndicator.removeFromSuperview()
            }
            else
            {
                imgView.image = UIImage(named: placeholder)
                myActivityIndicator.removeFromSuperview()
            }
        }
    }
    
class func requestPOSTURL(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }

    class func serviceCallXml(strURL : String, soapAction: secondryURLEnum, serviceName: String, method: String, params : Dictionary<String, AnyObject>, success: @escaping (XMLIndexer)->Void, failure: @escaping(NSError)-> Void)
    {
        let size = CGSize(width: 50, height:50)
        let presenter = PresenterViewController()
        
        presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        var parameters : String = ""
        for (key, param) in params {
            parameters = "\(parameters)<\(key)>\(param)</\(key)>"
        }
        
        let soapMessage =  "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\"><Body><\(serviceName) xmlns=\"\(soapAction.rawValue)\">\(parameters)</\(serviceName)></Body></Envelope>"
        
        let soapLength = String(soapMessage.characters.count)
        let theURL = URL(string: strURL)
        var mutableR = URLRequest(url: theURL!)
        
        // MUTABLE REQUEST
        debugPrint("\(soapAction.rawValue)/\(serviceName)")
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("\(soapAction.rawValue)/\(serviceName)", forHTTPHeaderField: "SOAPAction")
        mutableR.addValue(soapLength, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = method
        mutableR.httpBody = soapMessage.data(using: String.Encoding.utf8)
        mutableR.cachePolicy = URLRequest.CachePolicy.reloadRevalidatingCacheData
        
        Alamofire.request(mutableR).responseString { response in
            
            presenter.stopAnimating()
//            print(response.response ?? "no response")
//            print(response.data ?? "no data")
//            print(response.result)
            
            if response.result.isSuccess {
                 if let XML = response.result.value {
                    let xmlObj = SWXMLHash.parse(XML)
                    success(xmlObj)
                }
            }
            
            if response.result.isFailure {
                presenter.stopAnimating()
                let error : Error = response.result.error!
                failure(error as NSError)
            }
        }
    }

    
class func serviceCall(urlEnum: URLRequestConvertible,  success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        
         let size = CGSize(width: 50, height:50)
         let presenter = PresenterViewController()
        
        presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
       
        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
           presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                if resJson["status"].stringValue == "Failed" {
                     success(resJson)
                    
                   Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusDesc"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                       print("is this working?")
                    
                    if resJson["statusDesc"].stringValue == "Invalid Session!" {
                        
                        let delegate = UIApplication.shared.delegate as! AppDelegate
                        
                        let navCon = leftMenu.getMainViewController(sideDrawer: delegate.drawerController!)
                        
                        navigation.setViewControllerArray(vcIdentifier: "LoginViewController", navCon: navCon)
                        
                        
                        
                        
                    }

                    
                    })
                }else{
                success(resJson)
                    
                   
                }
            }
            if responseObject.result.isFailure {
               let error : NSError = responseObject.result.error! as NSError
                if let err = responseObject.result.error as? NSError , err.code == -1009 {
                    // no internet connection
                    
                    print("hey bro! you r not connected to internet")
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Internet connection appears to be offline.", btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        failure(error)
                    })
                    
                } else {
                   // "Server not responding, please try again later."
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: error.localizedDescription, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                         failure(error)
                    })
                }
                
              //--ww  failure(error)
            }
        }
    }
    
    class  func serviceCallWithOptionalLoader(urlEnum: URLRequestConvertible, showLoader :Bool,  success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void ) {
         let presenter = PresenterViewController()
       if showLoader == true {
            let size = CGSize(width: 50, height:50)
           presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        }
        
       Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            
            presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                if resJson["status"].stringValue == "Failed" {
                    success(resJson)
                    
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusDesc"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        if resJson["statusDesc"].stringValue == "Invalid Session!" {
                            
                             let delegate = UIApplication.shared.delegate as! AppDelegate
                            
                            let navCon = leftMenu.getMainViewController(sideDrawer: delegate.drawerController!)
                            
                            navigation.setViewControllerArray(vcIdentifier: "LoginViewController", navCon: navCon)
                            
                           
                        
                        
                        }
                    })
                }else{
                    success(resJson)
                    
                    
                }
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error! as NSError
                if let err = responseObject.result.error as? NSError , err.code == -1009 {
                    // no internet connection
                    
                    print("hey bro! you r not connected to internet")
                } else {
                    // other failures
                }
                
                failure(error)
            }
        }
    }
    
}

class PresenterViewController: UIViewController, NVActivityIndicatorViewable { }
