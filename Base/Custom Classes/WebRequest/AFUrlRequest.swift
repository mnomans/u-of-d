//
//  AFUrlRequest.swift
//  Skeleton
//
//  Created by Waqas Ali on 04/09/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//


import Alamofire

public enum AFUrlRequest: URLRequestConvertible {
    
    static let baseURLPath = Constants.baseURL
  

    static let user = "userName"
    static let password = "asfaifi"
    static let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
    static let base64Credentials = credentialData.base64EncodedString(options: [])
    static let authenticationToken = "Basic \(base64Credentials)"
    //static let authenticationToken = "Basic xxx"
    
    case LoginUserDotNet(userName: String, password: String)
    case overTime()

    
    
    // MARK: URLRequestConvertible
    
    public func asURLRequest() throws -> URLRequest {
    //    var encoding =  URLEncoding.default
      
        
        let result: (path: String, method: HTTPMethod, parameters: [String: AnyObject], encodeType : ParameterEncoding) = {
            switch self {
                
            case .LoginUserDotNet(let userName, let password):
          let params = [
                        "username" : userName ,
                        "password" : password ,
                        "DeviceId" : "123" ,
                        "DevicePlatform" : "IOS"
          ]
         
                return ("getlivedata", .post, params as [String : AnyObject], URLEncoding.default )
            
                
            case.overTime():
                let params = ["userName":"asfaifi"]
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]

                
                
                print(params)
                 return ("OvertimeInquiry", .post, params as [String : AnyObject], URLEncoding.default )
                
            }
        }()
        
      
        let url = try AFUrlRequest.baseURLPath.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue(AFUrlRequest.authenticationToken, forHTTPHeaderField: "Authorization")

        urlRequest.timeoutInterval = TimeInterval(100 * 10)

        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")

        
        let encoding = result.encodeType
      // return urlRequest
         return try encoding.encode(urlRequest, with: result.parameters)
    }
    
}



