//
//  printLog.swift
//  Base
//
//  Created by Waqas Ali on 06/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import Foundation

//func printLog(_ arg: Any...) {
//    
//     print(arg)
 // items.map { "*\($0)" }.joined(separator: separator)
//    
//}

public func printLog(_ items: Any..., separator: String = " ", terminator: String = "\n") {
  let output = items.map { "\($0)" }.joined(separator: separator)
    Swift.print(output, terminator: terminator)
}
