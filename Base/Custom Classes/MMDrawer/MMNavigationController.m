// Copyright (c) 2013 Mutual Mobile (http://mutualmobile.com/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#import "MMNavigationController.h"
#import "UIViewController+MMDrawerController.h"

@interface MMNavigationController ()

@end

@implementation MMNavigationController

-(UIStatusBarStyle)preferredStatusBarStyle{
    if(self.mm_drawerController.showsStatusBarBackgroundView){
        return UIStatusBarStyleLightContent;
    }
    else {
        return UIStatusBarStyleDefault;
    }
}

//-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    if ([Singleton ifAppLanguageIs:ArabicLanguage])
//    {
//        [self pushControllerAnimation];
//        [super pushViewController:viewController animated:NO];
//    }
//    else
//    {
//        [super pushViewController:viewController animated:YES];
//    }
//    
//}

//- (UIViewController *)popViewControllerAnimated:(BOOL)animated; // Returns the popped controller.
//{
//    if ([Singleton ifAppLanguageIs:ArabicLanguage])
//    {
//        [self popControllerAnimation];
//        return [super popViewControllerAnimated:NO];
//    }
//    else
//    {
//        return [super popViewControllerAnimated:YES];
//    }
//}

- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated; // Pops view controllers until the one
{
    return [super popToViewController:viewController animated:animated];
}


//-(NSArray*)popToRootViewControllerAnimated:(BOOL)animated
//{
//    if ([Singleton ifAppLanguageIs:ArabicLanguage])
//    {
//        [self popControllerAnimation];
//        return [super popToRootViewControllerAnimated:NO];
//    }
//    else
//    {
//        return [super popToRootViewControllerAnimated:animated];
//    }
//}

-(void)pushControllerAnimation
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.45;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionFromLeft;
    [transition setType:kCATransitionPush];
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    [self.view.layer addAnimation:transition forKey:nil];
}

-(void)popControllerAnimation
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.45;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionFromRight;
    [transition setType:kCATransitionReveal];
    transition.subtype = kCATransitionFromRight;
    transition.delegate = self;
    [self.view.layer addAnimation:transition forKey:nil];
}

@end
