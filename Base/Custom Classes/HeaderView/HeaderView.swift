//
//  HeaderView.swift
//  Skeleton
//
//  Created by Waqas Ali on 12/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit


@objc protocol HeaderViewDelegate {
   
    @objc optional func headerViewLeftBtnDidClick(headerView : HeaderView) -> Void
    @objc optional func headerViewRightBtnDidClick(headerView : HeaderView) -> Void
    @objc optional func headerViewRightSecondBtnDidClick(headerView : HeaderView) -> Void
    @objc optional func headerViewMenuBtnDidClick(headerView: HeaderView)-> Void
}

public class HeaderView: UIView {
    
    var delegate: HeaderViewDelegate?
    @IBOutlet var leadingConstraintLeftBtn : NSLayoutConstraint?
    @IBOutlet var trailingConstraintRightBtn : NSLayoutConstraint?

   
    @IBOutlet weak var lblHeading: BaseUILabel!

    @IBOutlet weak var btnLeft: UIButton!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var btnRight: UIButton!
    
    //@IBOutlet var viewContainer: UIView!
    
    @IBOutlet weak var LineMenu: UIView!
    @IBOutlet weak var LineLeft: UIView!
    @IBOutlet weak var LineRight: UIView!
    
   
    private var _leftButtonImage : String? = nil
    var leftButtonImage : String?{
        
//        get {
//            return _leftButtonImage
//        }
        
        didSet
        {
            if (leftButtonImage != nil && (leftButtonImage  == _leftButtonImage))
            {return}
            //--
            _leftButtonImage = leftButtonImage
            //--
            
            if (_leftButtonImage != nil && _leftButtonImage != "")
            {
                self.btnLeft.isHidden = false
            // self.btnLeft.setImage(UIImage(named:_leftButtonImage! ), forState: .Normal)
                //Changed by Ahsan
                self.btnLeft.setImage(UIImage (setImageForPro:  _leftButtonImage!), for: .normal)
                LineLeft.isHidden = false
            }
            else{
                self.btnLeft.isHidden = true
                LineLeft.isHidden = true;
            }
        }
    }
  
    private var _rightButtonImage : String? = nil
    
    var rightButtonImage : String? {
               didSet {
                
                if (rightButtonImage != nil && (rightButtonImage  == _rightButtonImage))
                {return}
                //--
                _rightButtonImage = rightButtonImage
                //--
                
                if (_rightButtonImage != "")
                {
                    self.btnRight.isHidden = false
                    self.btnRight.setImage(UIImage (setImageForPro: _rightButtonImage!), for: .normal)
                    LineRight.isHidden = false
                }
                else{
                    self.btnRight.isHidden = true
                    LineRight.isHidden = true
                }

        }
    }
    
    private var _menuButtonImage : String? = nil
    
    var menuButtonImage : String? {
        didSet {
            
            if (menuButtonImage != nil && (menuButtonImage  == _menuButtonImage))
            {return}
            //--
            _menuButtonImage = menuButtonImage
            //--
            
            if (_menuButtonImage != "")
            {
                self.btnMenu.isHidden = false
                self.btnMenu.setImage(UIImage(named: _menuButtonImage!), for: .normal)
                LineMenu.isHidden = false
            }
            else{
                self.btnMenu.isHidden = true
                LineMenu.isHidden = true
            }
            
        }
    }
    
    private var _heading : String? = nil
    var heading : String?{
     
        didSet {
            _heading = heading;
            if(_heading != nil){
            self.lblHeading.isHidden = false
            self.lblHeading.text = heading
            }
            
        }
    }
  
    private var _bgColorVar : String?
    var bgColorVar : String?{
        didSet {
            _bgColorVar = bgColorVar
            if ((bgColorVar) != nil)
            {
                self.backgroundColor = UIColor(hexString: bgColorVar!)
                
            }

        }
    }
    
    var _isBackgroundColored : Bool?
    
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    var view:UIView!;
    
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    @IBAction func leftbuttonClicked(sender: UIButton!) -> Void {
        
         delegate?.headerViewLeftBtnDidClick?(headerView: self)
        
    }
    
    @IBAction func rightbuttonClicked(sender: UIButton!) -> Void {
////        
//        btnMenu.removeFromSuperview()
//        LineMenu.removeFromSuperview()
////        
//        btnLeft.removeFromSuperview()
//        LineLeft.removeFromSuperview()
//
//        //btnRight.removeFromSuperview()
//        //LineRight.removeFromSuperview()
//        
//        layoutIfNeeded()
        
          delegate?.headerViewRightBtnDidClick?(headerView: self)
    }

    @IBAction func menubuttonClicked(sender: UIButton!) -> Void {
        delegate?.headerViewMenuBtnDidClick?(headerView: self)
    }
    
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String  ) -> Void {
}
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, menuBtnImageName: String, bgColor: String  ) -> Void {
        
         rightButtonImage   = rightBtnImageName
         leftButtonImage    = leftBtnImageName
         menuButtonImage    = menuBtnImageName
         bgColorVar = bgColor
         heading = hText
    }
    

 
  
}
