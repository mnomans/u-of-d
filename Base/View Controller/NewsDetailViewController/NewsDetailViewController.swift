//
//  NewsDetailViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 10/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class NewsDetailViewController: HeaderViewController {

    @IBOutlet weak var sendBtnn: UIButton?
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var txtDesc : BaseUITextView?
    @IBOutlet weak var imgTop : UIImageView?
    @IBOutlet  var txtViewhHeightConstraints : NSLayoutConstraint!

    var isFirstTime : Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgTop?.image = nil
        // Do any additional setup after loading the view.
        
        isFirstTime = true
        
        
        if GlobalStaticMethods.isPhone(){
            self.txtViewhHeightConstraints?.constant = 170
            
            
        }else{
            self.txtViewhHeightConstraints?.constant = 250
            
        }
        sendBtnn?.addTarget(self, action:#selector(setHeight), for: .touchUpInside)
        

        
        sendBtnn?.layer.cornerRadius = 4.0;
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
        {
            sendBtnn?.layer.cornerRadius = 6.0;
        }
        
        sendBtnn?.layer.masksToBounds = true;
        
        self.updateHeaderWithHeadingText(hText: "NEWS DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        debugPrint("DATA \(Singleton.sharedInstance.newsDetail)")
        let data = Singleton.sharedInstance.newsDetail
        
        lblTitle?.text = data?["Title"].element?.text
        txtDesc?.setAttributedStringWithHtml(htmlStr: data?["Body"].element?.text ?? "")
        AFWrapper.LoadImage(strUrl: data?["Image"].element?.text ?? "", imgView: self.imgTop!, placeholder: "")
    }

    @IBAction func readMore()
    {
        
    }
    
    
    func setHeight()  {
        
        if isFirstTime == true{
            
            txtViewhHeightConstraints.isActive = false
            isFirstTime = false
            sendBtnn?.setTitle("Read Less", for: .normal)
        }
        else{
            
            txtViewhHeightConstraints.isActive = true
            isFirstTime = true
            sendBtnn?.setTitle("Read More ", for: .normal)
            
        }
        
        
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
