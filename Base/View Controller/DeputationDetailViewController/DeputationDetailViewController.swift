//
//  DeputationDetailViewController.swift
//  
//
//  Created by Mohammed Noman Siddiqui on 11/12/2016.
//
//

import UIKit
import SwiftyJSON

class DeputationDetailViewController: HeaderViewController,UICollectionViewDelegate
,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    var arrayInfo = ["Employee Name","Employee Number","Payroll No","Desicion No","Number Of Days","Month","Year","Payroll Type","Submit Date"]
    var arrdata = ["Employee Name","Employee Number","Payroll No","Desicion No","Number Of Days","Month","Year","Payroll Type"]
    var dataArray : JSON?
    var index : Int?
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    @IBOutlet  var topHeightConstraint: NSLayoutConstraint?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dataArray)
        self.updateHeaderWithHeadingText(hText: "DEPUTATION DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        self.setDataforDetail()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrdata.count + 1
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = DeputationDetailCollectionViewCell()
        
        if indexPath.row == 0{
            let headerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as! DeputationDetailCollectionViewCell
            
            headerCell.lblName.text = dataArray?["EmpFullName"].stringValue
            headerCell.lblInfoStat.text = self.arrayInfo[indexPath.row ]
            
            return headerCell
            
            
        }
        else{
            
            
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DeputationDetailCollectionViewCell
            
            cell.lblInfoStat.text = self.arrayInfo[indexPath.row ]
            cell.lblName.text = arrdata[indexPath.row - 1]
            // Use the outlet in our custom class to get a reference to the UILabel in the cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        
        // Set cell width to 100%
        
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if indexPath.row == 0{
                    return CGSize(width: collectionViewWidth - 40, height: 200)
                    
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 150)
                    
                }
            }
            else{
                
                
                if indexPath.row == 0{
                    return CGSize(width: collectionViewWidth - 40, height: 160)
                    
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 100)
                    
                }
                
            }
        }else{
            
            if indexPath.row == 0{
                return CGSize(width: collectionViewWidth - 40, height: 100)
                
            }else{
                return CGSize(width: collectionViewWidth/2 - 20.5, height: 60)
                
            }
            
            
        }

               
        
    }
    
    
    func setDataforDetail()  {
        
        arrdata[0] = (dataArray?["EmpNumber"].stringValue)!
        arrdata[1] = (dataArray?["DeputationInfoList"][index!]["PayrollNo"].stringValue)!
        arrdata[2] = (dataArray?["DeputationInfoList"][index!]["DecisionNo"].stringValue)!
        arrdata[3] = (dataArray?["DeputationInfoList"][index!]["NumberofDays"].stringValue)!
        arrdata[4] = (dataArray?["DeputationInfoList"][index!]["Month"].stringValue)!
        arrdata[5] = (dataArray?["DeputationInfoList"][index!]["Year"].stringValue)!
        arrdata[6] = (dataArray?["DeputationInfoList"][index!]["PayrollType"].stringValue)!
        arrdata[7] = (dataArray?["DeputationInfoList"][index!]["SubmitDate"].stringValue)!
        
    }
    
    

}
