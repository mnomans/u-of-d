//
//  SelectSubjectViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 15/01/2017.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit

class SelectSubjectViewController: HeaderViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView : UITableView?
    
    var dataArray = ["Linear Algebra", "Introduction to Computer Science", "Data Mining", "Deep Learning", "English Compulsary", "Linear Algebra", "Introduction to Computer Science", "Data Mining", "Deep Learning", "English Compulsary","Linear Algebra", "Introduction to Computer Science", "Data Mining", "Deep Learning", "English Compulsary"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "SELECT SUBJECT", rightBtnImageName: "", leftBtnImageName: "gen-arrow-back", menuBtnImageName: "", bgColor: Constants.COLOR_TOP_BAR)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        navigation.goToViewController(viewControllerIdentifier: VC_COURSE_INQUIRY, animation: true)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "select-term-cell", for: indexPath) as! SelectTermTableViewCell
        
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL_ALTERNATE)
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL);
        }
        
        cell.lblBtn?.text = dataArray[indexPath.row]
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
