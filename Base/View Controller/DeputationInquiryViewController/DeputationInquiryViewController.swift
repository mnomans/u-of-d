//
//  DeputationInquiryViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 12/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class DeputationInquiryViewController: UIViewController ,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    var arrayInfo = ["Employee Name","Employee Number","Payroll No"]

    
    //"Desicion No"
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = DeputationinquiryCollectionViewCell()
        if indexPath.row == 0{
            let headerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as! DeputationinquiryCollectionViewCell
            
            headerCell.lblNameStat.text = self.arrayInfo[indexPath.row ]
            headerCell.lblNameService.text = self.arrayInfo[indexPath.row ]
            return headerCell
            
        }
        else if indexPath.row == 2{
          let headingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeadingCell", for: indexPath as IndexPath) as! DeputationinquiryCollectionViewCell

                return headingCell
        }
        else if indexPath.row == 1{
            var summaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SummaryCell", for: indexPath as IndexPath) as! DeputationinquiryCollectionViewCell
            
            summaryCell.BtnDetail?.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
            summaryCell.BtnDetail?.tag = indexPath.row
            
            return summaryCell
        }
        
        else{
        
        
        }
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        return cell
    }
    
    func btnClicked(sender: UIButton)
    {
        //write the task you want to perform on buttons click event..
        print("item at \(sender.tag)")
        navigation.goToViewController(viewControllerIdentifier: VC_DEPUTATION_DETAIL, animation: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let height = self.view.frame.height
        
        
        // Set cell width to 100%
        
        
//        if constants.deviceType.IS_IPHONE_5{
//            return CGSize(width: collectionViewWidth/2 - 0.5, height: height * 0.18)
//        }
//        else if constants.deviceType.IS_IPHONE_6{
//            return CGSize(width: collectionViewWidth/2 - 0.5, height: height * 0.1862)
//            
//        }
//        else{
        //    return CGSize(width: collectionViewWidth, height: height * 0.1916)
      //  }
        
        
        
        
        if indexPath.row == 0{
            return CGSize(width: collectionViewWidth, height: 100)

            
        }
        else if indexPath.row == 2{
            return CGSize(width: collectionViewWidth , height: 30)

        }
        else if indexPath.row == 1{
            return CGSize(width: collectionViewWidth , height: 40)

        }
            
        else{
            return CGSize(width: collectionViewWidth , height: 60)

            
        }
        
        
        
    }

}
