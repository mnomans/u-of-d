//
//  ChooseLanguageViewController.swift
//  Base
//
//  Created by noman  on 12/31/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class ChooseLanguageViewController: HeaderViewController {

    @IBOutlet weak var btnAr : UIButton?
    @IBOutlet weak var btnEn : UIButton?
    @IBOutlet weak var container : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(Singleton.showBackOnLangPage)
        {
            self.updateHeaderWithHeadingText(hText: "LANGUAGE", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:"", bgColor: Constants.COLOR_TOP_BAR)
        }
        else
        {
            self._headerView.removeFromSuperview()
            
        }
        
        btnEn?.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
        
        btnAr?.tada(nil)
        
        btnEn?.tada({
            
        })
    }
    
    func btnClicked(sender: UIButton)
    {
        if(Singleton.showBackOnLangPage)
        {
            navigation.popViewController()
        }
        else
        {
            navigation.goToViewController(viewControllerIdentifier: VC_DASHBOARD, animation: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
