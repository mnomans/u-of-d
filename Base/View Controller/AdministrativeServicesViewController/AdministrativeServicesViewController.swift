//
//  AdministrativeServicesViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 15/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

enum AdminEnum : Int {
    case SalarySlip = 0,
    DeputationInquiry,
    OvertimeInquiry,
    VacationInquiry,
    CourseRequest,
    LeaveRequest,
    BookingRequest,
    DeputationRequest,
    ClearanceRequest,
    EngagementRequest
}

class AdministrativeServicesViewController: HeaderViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var dataArray = [["title":"Salary \nSlip", "img":"as-icon-salary-slip"],
                     ["title":"Deputation \nInquiry", "img":"as-icon-deputation-inquiry"],
                     ["title":"Overtime \nInquiry", "img":"as-icon-overtime-inquiry"],
                     ["title":"Vacation \nInquiry", "img":"as-icon-vacation-inquiry"],
                     ["title":"Course \nInquiry", "img":"as-icon-course-inquiry"],
                     ["title":"Leave \nRequest", "img":"as-icon-leave-request"],
                     ["title":"Booking \nRequest", "img":"as-icon-booking-request"],
                     ["title":"Deputation \nRequest", "img":"as-icon-deputation-request"],
                     ["title":"Clearance \nRequest", "img":"as-icon-clearance-request"],
                     ["title":"Engement \nRequest", "img":"as-icon-engagement-request"]]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        self.updateHeaderWithHeadingText(hText: "ADMIN SERVICES", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
    }
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.dataArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashboardCollectionViewCell
        
        var data = dataArray[indexPath.row]
        cell.btnIcon.setImage(UIImage(named: data["img"]!), for: .normal)
        cell.lblBtn.text = data["title"];
        cell.btnIcon.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
        cell.btnIcon.tag = indexPath.row
        
        return cell
    }
    
    func btnClicked(sender: UIButton)
    {
        //write the task you want to perform on buttons click event..
        print("item at \(sender.tag)")
        
        let value = AdminEnum(rawValue: sender.tag)
        
        switch  value{
            
        case AdminEnum.SalarySlip?:
            navigation.goToViewController(viewControllerIdentifier: VC_SALARY_SLIP, animation: true)
            break
            
        case AdminEnum.DeputationInquiry?:
            navigation.goToViewController(viewControllerIdentifier: VC_DEPUTATION_INQUIRY, animation: true)
            break
            
        case AdminEnum.OvertimeInquiry?:
            navigation.goToViewController(viewControllerIdentifier: VC_OVERTIME, animation: true)
            break
            
        case AdminEnum.VacationInquiry?:
            navigation.goToViewController(viewControllerIdentifier: VC_VACATION_INQUIRY, animation: true)
            break
            
        case AdminEnum.LeaveRequest?:
            navigation.goToViewController(viewControllerIdentifier: VC_LEAVE_REQUEST, animation: true)
            break
            
        case AdminEnum.BookingRequest?:
            navigation.goToViewController(viewControllerIdentifier: VC_BOOKING_REQUEST, animation: true)
            break
            
        case AdminEnum.DeputationRequest?:
            navigation.goToViewController(viewControllerIdentifier: VC_DEPUTATION_REQUEST, animation: true)
            break
            
        case AdminEnum.ClearanceRequest?:
            navigation.goToViewController(viewControllerIdentifier: VC_CLEARANCE_REQUEST, animation: true)
            break
            
        case AdminEnum.EngagementRequest?:
            navigation.goToViewController(viewControllerIdentifier: VC_ENGAGEMENT_REQUEST, animation: true)
            break
            
        case AdminEnum.CourseRequest?:
            navigation.goToViewController(viewControllerIdentifier: VC_COURSE, animation: true)
            break
            
        default: break
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var aRatio : CGFloat = 1.4
        var spacer : CGFloat = 0.0
        var variant: CGFloat = 50.0
        
        if(GlobalStaticMethods.isPad())
        {
            aRatio = 1.23
            spacer = 57.0
            variant = 35.0
        }
        
        let width  : CGFloat = (self.collectionView.frame.width-50)/3
        let height : CGFloat = (aRatio * width) - spacer
        
        return CGSize(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
