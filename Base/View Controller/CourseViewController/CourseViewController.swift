//
//  CourseViewController.swift
//  Base
//
//  Created by noman  on 1/14/17.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit
import SwiftyJSON

class CourseViewController: HeaderViewController ,UITableViewDataSource ,UITableViewDelegate{
    
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var tblView : UITableView?
    
    
    var dataArray : JSON?
    
    
    var arrData : [String]?
    var arrDataStatic : Array<String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceCall()
        
        arrData = ["","","","",""]
        
        
        arrDataStatic = ["Employee No","Employee Name"]
        
        tblView?.estimatedRowHeight = 140
        tblView?.rowHeight = UITableViewAutomaticDimension
        tblView?.separatorColor = UIColor (hexString: "F1ECD9")
        
        tblView?.preservesSuperviewLayoutMargins = false
        tblView?.separatorInset = UIEdgeInsets.zero
        tblView?.layoutMargins = UIEdgeInsets.zero
        
        
        
        self.updateHeaderWithHeadingText(hText: "COURSE INQUIRY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        
        // Do any additional setup after loading the view.
    }
    func serviceCall()  {
        
        AFWrapper.requestPOSTURL(Constants.baseURL + Constants.SERVICE_COURSEINQUIRY, params: ["userName":"Asfaifi"], headers: Constants.headerArray, success: { (JSON) in
            debugPrint("DATA \(JSON)")
            
            // self.dataArray = JSON.arrayValue as! [AnyObject]
            self.dataArray = JSON["d"]
            print("JSON: \(self.dataArray)")
            
            
            self.tblView?.delegate = self
            self.tblView?.dataSource = self
            self.tblView?.reloadData()
            
            
            
        }) { (Error) in
            debugPrint("Error \(Error)")
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 50
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 30
                
            }
        }
    }
    
    
    //  MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! CourseTableViewCell
        
        headerCell.headerLbl?.text = "Summary"
        
        return headerCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
        
    }
    
    
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if section == 0 {
                    return 0
                    
                }else{
                    
                    return 130
                }
                
            }else{
                
                if section == 0 {
                    return 0
                    
                }
                
                
                return 100
            }
            
            
        }
            
            
        else{
            if section == 0 {
                return 0
                
            }
            else{
                
                return 70
            }
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return 2
        }
        else{
            if (dataArray?["CourseInfoList"] .count)! > 0{
                return (dataArray!["CourseInfoList"] .count + 1)
                
            }else{
                return 0
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = CourseTableViewCell()
        
        if indexPath.section == 0{
            var arrData = ["",""]
            
            arrData[0] = (dataArray?["EmpNumber"].stringValue)!
            arrData[1] = (dataArray?["EmpFullName"].stringValue)!

            cell = tableView.dequeueReusableCell(withIdentifier: "CellWithOutSpace", for: indexPath) as! CourseTableViewCell
            cell.empLbl.text = arrDataStatic?[indexPath.row]
            cell.empValuelLbl.text = arrData[indexPath.row]
            
        }
        else {
          
            if(indexPath.row == 0)
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! CourseTableViewCell
                
                cell.headernameLbl.text = "Name"
                cell.headerlocationLbl.text = "Location"
                cell.headerdurationLbl.text = "Duration"
                cell.headercpointLbl.text = "Cpoint"
            
            }
            else{
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell4Label", for: indexPath) as! CourseTableViewCell
                
                
                cell.nameLbl.text = dataArray?["CourseInfoList"][indexPath.row - 1]["Name"].stringValue
                cell.locationLbl.text = dataArray?["CourseInfoList"][indexPath.row - 1]["Location"].stringValue
                cell.durationLbl.text = dataArray?["CourseInfoList"][indexPath.row - 1]["Duration"].stringValue
                cell.cpointLbl.text = dataArray?["CourseInfoList"][indexPath.row - 1]["CPoint"].stringValue

                
                if indexPath.row % 2 == 0{
                    
                    cell.nameLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.durationLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.locationLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.cpointLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    
                    
                }else{
                    
                    cell.nameLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.durationLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.locationLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.cpointLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    
                    
                }
                
                
                
            }
            
            
            
            // cell.configureCell(cell: cell, forRowAtIndexPath: indexPath as NSIndexPath, lblArray: arrData!)
            
        }
        
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if indexPath.section == 0 {
                    return 140
                    
                }else{
                    
                    return 70
                }
                
            }else{
                
                if indexPath.section == 0 {
                    return 120
                    
                }
                
                
                return 60
            }
            
            
        }
            
            
        else{
            if indexPath.section == 0{
                return 60
            }
            else{
                return 30
            }
        }
    }
}
