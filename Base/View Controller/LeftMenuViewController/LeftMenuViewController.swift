//
//  LeftMenuViewController.swift
//  Base
//
//  Created by Hamza Khan on 28/11/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

enum  LeftMenu: Int {
    case Dashboard = 0,
    RectorMsg,
    UniNews,
    UniHistory,
    UniEvents,
    Map,
    EmployeeDirectory,
    SocialNetworks,
    ContactInfo
}

enum  ELeftMenu: Int {
    case Dashboard = 0,
    RectorMsg,
    UniNews,
    UniHistory,
    UniEvents,
    Map,
    AdminServices,
    ICT,
    CommunityServices,
    AcademicServices,
    EmployeeDirectory,
    SocialNetworks,
    ContactInfo,
    Logout
    
}

enum  SLeftMenu: Int {
    case Dashboard = 0,
    Profile,
    Academic,
    TimeTable,
    Grades,
    Attendance,
    Account,
    Events,
    ToDo,
    Advisors,
    Indicators,
    Enrollment,
    AboutUni,
    Logout
}


class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblView : UITableView?
    var dataArray = [Dictionary<String,String>]()
    
    @IBOutlet var lblUsername : UILabel?
    @IBOutlet var viewUser : UIView?
    
    var studentArray = [["title":"Dashboard", "img":"icon-dashboard"],
    ["title":"Profile", "img":"icon-profile"],
    ["title":"Academic", "img":"icon-academic"],
    ["title":"TimeTable", "img":"icon-timetable"],
    ["title":"Grades", "img":"icon-grades"],
    ["title":"Attendance", "img":"icon-attendance"],
    ["title":"Account", "img":"icon-account"],
    ["title":"Events", "img":"icon-events"],
    ["title":"ToDo", "img":"icon-todo"],
    ["title":"Advisor", "img":"icon-advisor"],
    ["title":"Indicators", "img":"icon-indicator"],
    ["title":"Enrollment", "img":"icon-enrollment"],
    ["title":"About University", "img":"icon-about-uni"],
    ["title":"Logout", "img":"icon-logout"]]
    
    var employeeArray = [["title":"Dashboard", "img":"icon-dashboard"],
    ["title":"Rector's Message", "img":"icon-msg"],
    ["title":"University News", "img":"icon-news"],
    ["title":"University History", "img":"icon-history"],
    ["title":"University Events", "img":"icon-events"],
    ["title":"University Map", "img":"icon-map"],
    ["title":"Administration Services", "img":"icon-services"],
    ["title":"ICT Services", "img":"icon-ict"],
    ["title":"Community Services", "img":"icon-community"],
    ["title":"Academic Services", "img":"icon-academic"],
    ["title":"Employee Directory", "img":"icon-directory"],
    ["title":"Social Network", "img":"icon-social"],
    ["title":"Contact Information", "img":"icon-contactinfo"],
    ["title":"Logout", "img":"icon-logout"]]
    
    var GeneralArray = [["title":"Dashboard", "img":"icon-dashboard"],
    ["title":"Rector's Message", "img":"icon-msg"],
    ["title":"University News", "img":"icon-news"],
    ["title":"University History", "img":"icon-history"],
    ["title":"University Events", "img":"icon-events"],
    ["title":"University Map", "img":"icon-map"],
    ["title":"Employee Directory", "img":"icon-directory"],
    ["title":"Social Network", "img":"icon-social"],
    ["title":"Contact Information", "img":"icon-contactinfo"]]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(Singleton.isStudentLoggedIn())
        {
            dataArray = studentArray
        }
        else if(Singleton.isEmployeeLoggedIn())
        {
            dataArray = employeeArray
            
        }
        else
        {
            dataArray = GeneralArray
        }
        
        if(Singleton.isEmployeeLoggedIn() || Singleton.isStudentLoggedIn())
        {
            let username = Singleton.sharedInstance.userDetail?["FullName"].element?.text
            lblUsername?.text = username ?? ""
        }
        else
        {
            viewUser?.removeFromSuperview()
        }
        
        tblView?.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(GlobalStaticMethods.isPad())
        {
            return 80.0
        }
        else
        {
            return 55.0
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "left-menu-cell", for: indexPath) as! MenuTableViewCell
        
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL_ALTERNATE)
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL);
        }
        
        let data = dataArray[indexPath.row]
        cell.lblBtn?.text = data["title"]
        cell.rowIcon?.image = UIImage(named: data["img"]!)
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(Singleton.isEmployeeLoggedIn())
        {
            let value = ELeftMenu(rawValue: indexPath.row)
            switch  value{
            case .Dashboard?:
                navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
                break
                
            case .RectorMsg?:
                navigation.goToViewController(viewControllerIdentifier: VC_RECTOR_MSG, animation: true)
                break
                
            case .UniNews?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_NEWS, animation: true)
                break
                
            case .UniEvents?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_EVENTS, animation: true)
                break
                
            case .UniHistory?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_HISTORY, animation: true)
                break
                
            case .EmployeeDirectory?:
                navigation.goToViewController(viewControllerIdentifier: VC_EMPLOYEE_DIRECTORY, animation: true)
                break
                
            case .Map?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_MAP, animation: true)
                break
                
            case .AdminServices?:
                navigation.goToViewController(viewControllerIdentifier: VC_ADMIN_SERVICES, animation: true)
                break
                
            case .ICT?:
                navigation.goToViewController(viewControllerIdentifier: VC_ICT_SERVICES, animation: true)
                break
                
            case .AcademicServices?:
                //navigation.goToViewController(viewControllerIdentifier: VC_ACADEMIC_INFO, animation: true)
                GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Coming soon in next version", vc: self)
                break
                
            case .CommunityServices?:
                //navigation.goToViewController(viewControllerIdentifier: VC_USER_LOGIN, animation: true)
                GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Coming soon in next version", vc: self)
                break
                
            case .SocialNetworks?:
                navigation.goToViewController(viewControllerIdentifier: VC_SOCIAL_NETWORK, animation: true)
                break
                
            case .ContactInfo?:
                navigation.goToViewController(viewControllerIdentifier: VC_CONTACT_INFO, animation: true)
                break
                
            case .Logout?:
                self.logout()
                break
                
            default: break
                
            }
        }
        else if(Singleton.isStudentLoggedIn())
        {
            let value = SLeftMenu(rawValue: indexPath.row)
            switch  value{
            case .Dashboard?:
                navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
                break
                
            case .Profile?:
                navigation.goToViewController(viewControllerIdentifier: VC_PROFILE, animation: true)
                break
                
            case .Academic?:
                navigation.goToViewController(viewControllerIdentifier: VC_ACADEMIC, animation: true)
                break
                
            case .TimeTable?:
                
                //GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "In progress", vc: self)
                navigation.goToViewController(viewControllerIdentifier: VC_TIMETABLE, animation: true)
                break
                
            case .Grades?:
                navigation.goToViewController(viewControllerIdentifier: VC_GRADES, animation: true)
                break
                
            case .Attendance?:
                navigation.goToViewController(viewControllerIdentifier: VC_ATTENDANCE, animation: true)
                break
                
            case .Account?:
                navigation.goToViewController(viewControllerIdentifier: VC_ACCOUNT, animation: true)
                break
                
            case .Events?:
                navigation.goToViewController(viewControllerIdentifier: VC_EVENTS, animation: true)
                break
                
            case .ToDo?:
                navigation.goToViewController(viewControllerIdentifier: VC_TODO, animation: true)
                break
                
            case .Advisors?:
                navigation.goToViewController(viewControllerIdentifier: VC_ADVISOR, animation: true)
                break
                
            case .Indicators?:
                navigation.goToViewController(viewControllerIdentifier: VC_INDICATOR, animation: true)
                break
                
            case .Enrollment?:
                navigation.goToViewController(viewControllerIdentifier: VC_ENROLLMENT, animation: true)
                break
                
            case .AboutUni?:
                navigation.goToViewController(viewControllerIdentifier: VC_ABOUT_UNI, animation: true)
                break
                
            case .Logout?:
                self.logout()
                break
                
                
            default: break
                
            }

        }
        else
        {
            let value = LeftMenu(rawValue: indexPath.row)
            switch  value{
            case .Dashboard?:
                navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
                break
                
            case .RectorMsg?:
                navigation.goToViewController(viewControllerIdentifier: VC_RECTOR_MSG, animation: true)
                break
                
            case .UniNews?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_NEWS, animation: true)
                break
                
            case .UniEvents?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_EVENTS, animation: true)
                break
                
            case .UniHistory?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_HISTORY, animation: true)
                break
                
            case .EmployeeDirectory?:
                navigation.goToViewController(viewControllerIdentifier: VC_EMPLOYEE_DIRECTORY, animation: true)
                break
                
            case .Map?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_MAP, animation: true)
                break
                
            case .SocialNetworks?:
                navigation.goToViewController(viewControllerIdentifier: VC_SOCIAL_NETWORK, animation: true)
                break
                
            case .ContactInfo?:
                navigation.goToViewController(viewControllerIdentifier: VC_CONTACT_INFO, animation: true)
                break
                
            default: break
                
            }
        }
        
        leftMenu.closeDrawer(sideDrawer: self.mm_drawerController)
    }
    
    
    func logout()
    {
        Singleton.sharedInstance.loginStatus = userLoginStatusEnum.kNotLoggedIn
        Singleton.sharedInstance.userDetail = nil
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadDashboard"), object: nil)
        navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
