//
//  WebViewViewController.swift
//  Base
//
//  Created by noman  on 1/5/17.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit

class WebViewViewController: HeaderViewController {

    @IBOutlet weak var webView : UIWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let username = (MMDrawerController.encryptData("moqasem").base64EncodedString())
        let strUrl = "\(Constants.URL_CLEARANCE_REQUEST)&usr=\(username)&lang=en"
        
        print(strUrl)
        
        webView?.loadRequest(URLRequest(url: URL(string: strUrl)!))
        
        self.updateHeaderWithHeadingText(hText: "Web View", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
