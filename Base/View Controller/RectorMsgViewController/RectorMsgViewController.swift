//
//  RectorMsgViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 10/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class RectorMsgViewController: HeaderViewController {

    @IBOutlet weak var sendBtnn: UIButton?
    @IBOutlet weak var imgTop : UIImageView?
    @IBOutlet weak var lblTop : UILabel?
    @IBOutlet weak var lblDesc : UILabel?
    @IBOutlet weak var lblLongDesc : BaseUITextView?

    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var contentView : UIView?

    @IBOutlet  var txtViewhHeightConstraints : NSLayoutConstraint!

    var isFirstTime : Bool?
    var dataXml : XMLIndexer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imgTop?.image = nil
        
        isFirstTime = true
        
        
        if GlobalStaticMethods.isPhone(){
            self.txtViewhHeightConstraints?.constant = 170


        }else{
            self.txtViewhHeightConstraints?.constant = 250
 
        }
        sendBtnn?.addTarget(self, action:#selector(setHeight), for: .touchUpInside)

        
        // Do any additional setup after loading the view.
        
        sendBtnn?.layer.cornerRadius = 4.0;
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
        {
            sendBtnn?.layer.cornerRadius = 6.0;
        }
        
        sendBtnn?.layer.masksToBounds = true;
        
        self.updateHeaderWithHeadingText(hText: "RECTOR MESSAGE", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        imgTop?.isHidden = true
        sendBtnn?.isHidden = true
        lblTop?.isHidden = true
        lblDesc?.isHidden = true
        lblLongDesc?.isHidden = true
        
        let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                      Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                    "lang":"1"]
        
        AFWrapper.serviceCallXml(strURL: Constants.URL_FEATURE, soapAction:.feature,  serviceName: Constants.SERVICE_RECTOR_MSG, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
            
            print("DATA MSG  \(xml["soap:Envelope"]["soap:Body"])")
            self.dataXml = xml["soap:Envelope"]["soap:Body"]["PresidentMessageResponse"]["PresidentMessageResult"]
            self.setData()
        })
        { (NSError) in
            print(NSError)
        }
    }
    
    func setData()
    {
        lblTop?.text = self.dataXml?["Title"].element?.text
        lblLongDesc?.setAttributedStringWithHtml(htmlStr: self.dataXml?["Body"].element?.text ?? "")
        AFWrapper.LoadImage(strUrl: self.dataXml?["Image"].element?.text ?? "", imgView: self.imgTop!, placeholder: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        imgTop?.flipFromLeft()
        sendBtnn?.flipFromLeft()
    
        lblTop?.fadeInn()
        lblDesc?.fadeInn()
        lblLongDesc?.fadeInn()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setHeight()  {
        
        if isFirstTime == true{
            
            txtViewhHeightConstraints.isActive = false
            isFirstTime = false
            sendBtnn?.setTitle("Read Less", for: .normal)
        }
        else{
        
            txtViewhHeightConstraints.isActive = true
            isFirstTime = true
            sendBtnn?.setTitle("Read More ", for: .normal)

        }
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
