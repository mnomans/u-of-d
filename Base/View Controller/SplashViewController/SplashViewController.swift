//
//  SplashViewController.swift
//  Base
//
//  Created by Hamza Khan on 28/11/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Alamofire

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        debugPrint("@2160000001mobappseckey2013".sha1())
        
        leftMenu.leftMenuPanGestureDisableEnable(vc: self, sideDrawer: self.mm_drawerController, containsBackImage: true)
        
        
        
        AFWrapper.requestPOSTURL("https://eservices.uod.edu.sa/eportal/api/ServiceAjax.svc/OvertimeInquiry", params: ["userName":"Asfaifi"], headers: ["Content-Type":"application/json"], success: { (JSON) in
            debugPrint("DATA \(JSON)")
            
        }) { (Error) in
            debugPrint("Error \(Error)")
        }
        
        
        
        
        
        
        
        
        //goToDashboard()
        // Do any additional setup after loading the view.
        //playVideo()
        
        //serviceCall()
        
//        let params = ["UserName":"myud",
//                      "PassWord":"my@ud#13$",
//                      "usr_user":"moqasem",
//                      "usr_pass":"ud@11223344"]
//        
//        AFWrapper.serviceCallXml(strURL: "https://appservices.uod.edu.sa/ldap.asmx", serviceName: "UserValid", method: "POST", params: params as Dictionary<String, AnyObject>, success: { (xml) in
//                print("DATA SPLASH  \(xml)")
//            })
//         { (NSError) in
//            print(NSError)
//        }
    }
    
    func serviceCall()
    {
//        let soapMessage =  "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\"><Body><UserValid xmlns=\"http://ud.edu.sa/api/ldap\"><usr_pass>ud@11223344</usr_pass><UserName>myud</UserName><PassWord>my@ud#13$</PassWord><usr_user>moqasem</usr_user></UserValid></Body></Envelope>"
//        
//        let soapLenth = String(soapMessage.characters.count)
//        let theUrlString = "https://appservices.uod.edu.sa/ldap.asmx"
//        let theURL = URL(string: theUrlString)
//        var mutableR = URLRequest(url: theURL!)
//        
//        // MUTABLE REQUEST
//        
//        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
//        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
//        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
//        mutableR.httpMethod = "POST"
//        mutableR.httpBody = soapMessage.data(using: String.Encoding.utf8)
        
        
        let params = ["UserName":"myud", "PassWord":"my@ud#13$", "usr_user":"moqasem", "usr_pass":"ud@11223344"]
        let mutableR = Services.createServiceRequest(params: params, serviceName: "UserValid", urlString:"https://appservices.uod.edu.sa/ldap.asmx" , method: "POST")
        
        Alamofire.request(mutableR)
            .responseString { response in
                print(response.response ?? "no response")
                print(response.data ?? "no data")
                print(response.result)
                
                if let XML = response.result.value {
                    //print("XML: \(XML)")
                    
                    let xmlObj = SWXMLHash.parse(XML)
                    print("DATA ORIGINAL  \(xmlObj)")
                    print("DATA  \(xmlObj["soap:Envelope"]["soap:Body"]["UserValidResponse"]["UserValidResult"].element?.text)")
                }
        }
    }
    
    private func playVideo() {
        
        var filename : String = "1242X2208"
        if(GlobalStaticMethods.isPad())
        {
            filename = "1536X2048"
        }
        
        guard let path = Bundle.main.path(forResource: filename, ofType:"mp4") else {
            debugPrint("\(filename) not found")
            return
        }
        
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
      
         _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.goToDashboard), userInfo: nil, repeats: false);
    }
    
    func goToDashboard()
    {
        navigation.goToViewController(viewControllerIdentifier: VC_DASHBOARD, animation: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
