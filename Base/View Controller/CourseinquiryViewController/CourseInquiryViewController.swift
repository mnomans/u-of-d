//
//  CourseInquiryViewController.swift
//  Base
//
//  Created by noman  on 1/14/17.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit

class CourseInquiryViewController: HeaderViewController ,UITableViewDataSource ,UITableViewDelegate{
    
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var tblView : UITableView?
    
    
    var arrData : Array<String>?
    var arrDataStatic : Array<String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrData = ["Summary","Summary","Summary","Summary","Balance Detail","Summary","Summary","Balance Detail","Summary","Summary","Balance Detail","Summary"]
        
        arrDataStatic = ["Employee No","Employee Name","Employee Name"]
        
//        tblView?.estimatedRowHeight = 140
//        tblView?.rowHeight = UITableViewAutomaticDimension
//        tblView?.separatorColor = UIColor (hexString: "F1ECD9")
        
        tblView?.separatorColor = UIColor.clear
        
        
        self.updateHeaderWithHeadingText(hText: "COURSE LIST", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
                
            }
        }
    }
    
    
    //  MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! CourseInquiryTableViewCell
        
      //  headerCell.headerLbl?.text = arrData?[section];
        return headerCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return (arrDataStatic?.count)!
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                    return 90
                
                 }else{
                
                return 75
            }
        }
        else{
            
              return 45
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        
            return 1
        //(arrData!.count)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        navigation.goToViewController(viewControllerIdentifier: VC_SELECT_CLASS, animation: true)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = CourseInquiryTableViewCell()
        
        cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CourseInquiryTableViewCell
        
        cell.selectionStyle = .none

                if indexPath.row % 2 == 0{
                    
//                    cell.netLbl.backgroundColor = UIColor (hexString: "fcf9f2")
//                    cell.NoOfdaysLbl.backgroundColor = UIColor (hexString: "fcf9f2")
//                    cell.PayrollLbl.backgroundColor = UIColor (hexString: "fcf9f2")
//                    cell.DesicionLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    
                    
                }else{
                    
//                    cell.netLbl.backgroundColor = UIColor (hexString: "fdfeff")
//                    cell.NoOfdaysLbl.backgroundColor = UIColor (hexString: "fdfeff")
//                    cell.PayrollLbl.backgroundColor = UIColor (hexString: "fdfeff")
//                    cell.DesicionLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    
                    
                }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
               
                    
                    return 670
                
                
            }else{
                
               
                
                return 560
            }
            
            
        }
        else{
                return 340
            
            
        }
    }
}
