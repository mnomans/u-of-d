//
//  SalarySlipViewController.swift
//  Base
//
//  Created by noman  on 12/23/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class SalarySlipViewController: HeaderViewController ,UITableViewDataSource ,UITableViewDelegate{
    
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var topViewBtnHeightConstraint : NSLayoutConstraint?

    @IBOutlet var tblView : UITableView?
    var arrData : Array<String>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrData = ["Basic","Transportation Allowance","Other Allowance"]
        
        
        tblView?.estimatedRowHeight = 140
        tblView?.rowHeight = UITableViewAutomaticDimension
        tblView?.separatorColor = UIColor (hexString: "F1ECD9")

        tblView?.preservesSuperviewLayoutMargins = false
        tblView?.separatorInset = UIEdgeInsets.zero
        tblView?.layoutMargins = UIEdgeInsets.zero
        
        
        self.updateHeaderWithHeadingText(hText: "SALARY SLIP", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
 
     -(void)viewWillAppear:(BOOL)animated
     {
     CGFloat tableBorderLeft = 20;
     CGFloat tableBorderRight = 20;
     
     CGRect tableRect = self.view.frame;
     tableRect.origin.x += tableBorderLeft; // make the table begin a few pixels right from its origin
     tableRect.size.width -= tableBorderLeft + tableBorderRight; // reduce the width of the table
     tableView.frame = tableRect;
     }
 
 */
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
//        let tableBorderLeft : CGFloat = 500
//        let tableBorderRight : CGFloat = 500
//        
//        var tableRect : CGRect = self.view.frame
//
//        tableRect.origin.x += tableBorderLeft; // make the table begin a few pixels right from its origin
//        tableRect.size.width -= tableBorderLeft + tableBorderRight; // reduce the width of the table
//        tblView!.frame = tableRect
//        
//        
//        
        
        
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 50
                topViewBtnHeightConstraint?.constant = 120
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 30
                topViewBtnHeightConstraint?.constant = 90

            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "Cell2LabelHeader") as! SalarySlipTableViewCell
        
        headerCell.HeaderAllowanceNameLBL?.text = "AllowanceName"
        headerCell.HeaderAllowanceAmountLBL?.text = "AllowanceAmount"
        return headerCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
        
    }
    
    
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro(){
                
                return 60
                
                
            }else{
                
               return 50
            }
        }
        
        else{
        return 30
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
       
            return (arrData!.count)
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = SalarySlipTableViewCell()
        
        
            
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell2Label", for: indexPath) as! SalarySlipTableViewCell
        
            cell.selectionStyle = .none

            cell.AllowanceAmountLBL.text = arrData?[indexPath.row]
            cell.AllowanceNameLBL.text = arrData?[indexPath.row]

        
            if indexPath.row % 2 == 0{
                    
                cell.ViewCell.backgroundColor = UIColor (hexString: "fdfeff")

                
                }else{
                
                cell.ViewCell.backgroundColor = UIColor (hexString: "fcf9f2")

                }
                
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro(){
                
                return 60
                
                
            }else{
                
                return 50
            }
        }
            
        else{
            return 30
        }
        
    }

}
