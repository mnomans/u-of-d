//
//  AttendanceViewController.swift
//  Base
//
//  Created by noman  on 12/25/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class AttendanceViewController: HeaderViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView : UITableView?
    
    var dataArray = [["title":"Rector's Message", "img":"icon-msg"],
                     ["title":"University News", "img":"icon-news"],
                     ["title":"University History", "img":"icon-history"],
                     ["title":"University Events", "img":"icon-events"],
                     ["title":"University Map", "img":"icon-map"],
                     ["title":"Administration Services", "img":"icon-services"],
                     ["title":"Community Services", "img":"icon-community"],
                     ["title":"Academic Services", "img":"icon-academic"],
                     ["title":"Employee Directory", "img":"icon-directory"],
                     ["title":"Social Network", "img":"icon-social"],
                     ["title":"Contact Information", "img":"icon-contactinfo"],
                     ["title":"Logout", "img":"icon-logout"]];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "ATTENDANCE INFORMATION", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceCell", for: indexPath) as! AttendanceTableViewCell
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
