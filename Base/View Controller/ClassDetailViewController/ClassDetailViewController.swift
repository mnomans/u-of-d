//
//  ClassDetailViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 16/01/2017.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit

class ClassDetailViewController: HeaderViewController,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    var arrayInfo = [["title":"EDCU 103 +Name", "value":"", "cellType": "f"],
                     ["title":"Course Id", "value":"80125", "cellType": "fns"],
                     ["title":"Course Name", "value":"EDCU", "cellType": "fns"],
                     ["title":"Class No", "value":"2556", "cellType": "fns"],
                     ["title":"Catalog No", "value":"250N", "cellType": "fns"],
                     ["title":"Section", "value":"T02", "cellType": "fns"],
                     ["title":"Component", "value":"December", "cellType": "fns"],
                     ["title":"Status", "value":"Open", "cellType": "fns"],
                     ["title":"Instructor", "value":"Khalid Bin Saeed", "cellType": "fns"],
                     ["title":"Class Login", "value":"DMM04F", "cellType": "fns"],
                     ["title":"Class Date", "value":"02-05-2015/02-05-2016", "cellType": "fns"],
                     ["title":"Class Time", "value":"Active", "cellType": "fns"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //  collectionView.delegate = self
        // collectionView.dataSource = self
        
        self.updateHeaderWithHeadingText(hText: "CLASS DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 50
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 30
                
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = SelectClassCollectionViewCell()
        
        let data = arrayInfo[indexPath.row]
        
        if data["cellType"] == "f"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as! SelectClassCollectionViewCell
        }
        else if data["cellType"] == "bs"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BtnCell", for: indexPath as IndexPath) as! SelectClassCollectionViewCell
        }
        else
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCellNoSpace", for: indexPath as IndexPath) as! SelectClassCollectionViewCell
        }
        
        cell.lblName?.text = data["title"]
        cell.lblInfoStat?.text = data["value"]
        
        let single = Singleton.sharedInstance
        if single.classStatus == .kDropClass
        {
            cell.btnClass?.setTitle("DROP CLASS", for: UIControlState.normal)
        }
        else
        {
            cell.btnClass?.setTitle("ADD CLASS", for: UIControlState.normal)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let data = arrayInfo[indexPath.row]
        
        // Set cell width to 100%
        
        
        if  GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro(){
                if data["cellType"] == "f"{
                    return CGSize(width: collectionViewWidth - 40, height: 80)
                    
                }
                else if data["cellType"] == "bs"{
                    return CGSize(width: collectionViewWidth - 40, height: 100)
                }
                else if(data["cellType"] == "fns"){
                    return CGSize(width: collectionViewWidth - 40, height: 60) /// only this cell is in use
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 60)
                    
                }
            }
            else{
                
                if data["cellType"] == "f"{
                    return CGSize(width: collectionViewWidth - 40, height: 70)
                    
                }
                else if data["cellType"] == "bs"{
                    return CGSize(width: collectionViewWidth - 40, height: 90)
                }
                else if(data["cellType"] == "fns"){
                    return CGSize(width: collectionViewWidth - 40, height: 50)
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 50)
                    
                }
            }
            
        }else{
            if data["cellType"] == "f"{
                return CGSize(width: collectionViewWidth - 40, height:50)
                
            }
            else if data["cellType"] == "bs"{
                return CGSize(width: collectionViewWidth - 40, height: 60)
            }
            else if(data["cellType"] == "fns"){
                return CGSize(width: collectionViewWidth - 40, height: 30)
            }else{
                return CGSize(width: collectionViewWidth/2 - 20.5, height: 30)
                
            }
        }
    }
    
    
}
