//
//  PersonalInformationViewController.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class PersonalInformationViewController: HeaderViewController ,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?

    
    var arrayInfo = [["title":"Primary Name", "value":"Mustafa Mohammed Qaseem", "cellType": "f"],
                     ["title":"First Name", "value":"Mustafa", "cellType": "h"],
                     ["title":"Father Name", "value":"Mohammed", "cellType": "h"],
                     ["title":"Family Name", "value":"Qaseem", "cellType": "h"],
                     ["title":"Date of Birth", "value":"15-12-1985", "cellType": "h"],
                     ["title":"Parent/Guardian", "value":"Mohammed", "cellType": "h"],
                     ["title":"Mobile No.", "value":"00939034874", "cellType": "h"],
                     ["title":"Email", "value":"mqaseem@hotmail.com", "cellType": "fns"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //  collectionView.delegate = self
        // collectionView.dataSource = self
        
        self.updateHeaderWithHeadingText(hText: "PROFILE", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 20
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 10
                
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = PersonalInformationCollectionViewCell()
        
        let data = arrayInfo[indexPath.row]
        
        if data["cellType"] == "f"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as! PersonalInformationCollectionViewCell
        }
        else if(data["cellType"] == "fns")
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCellNoSpace", for: indexPath as IndexPath) as! PersonalInformationCollectionViewCell
        }
        else{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! PersonalInformationCollectionViewCell
        }
        
        cell.lblName.text = data["value"]
        cell.lblInfoStat.text = data["title"]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let data = arrayInfo[indexPath.row]
        // Set cell width to 100%

        if  GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro(){
                if data["cellType"] == "f"{
                    return CGSize(width: collectionViewWidth - 40, height: 160)
                    
                }
                else if(data["cellType"] == "fns"){
                    return CGSize(width: collectionViewWidth - 40, height: 120)
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 120)
                    
                }
            }
            else{
                
                if data["cellType"] == "f"{
                    return CGSize(width: collectionViewWidth - 40, height: 140)
                    
                }
                else if(data["cellType"] == "fns"){
                    return CGSize(width: collectionViewWidth - 40, height: 100)
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 100)
                    
                }
            }
            
        }
        else{
        
        if data["cellType"] == "f"{
            return CGSize(width: collectionViewWidth - 40, height: 100)
            
        }
        else if(data["cellType"] == "fns"){
            return CGSize(width: collectionViewWidth - 40, height: 60)
        }else{
            return CGSize(width: collectionViewWidth/2 - 20.5, height: 60)
            
            }
        }
    }
    
    
}
