//
//  LoginViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 11/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class LoginViewController: HeaderViewController, UITextFieldDelegate {

    @IBOutlet weak var userrView : UIView?
    @IBOutlet weak var passsView : UIView?
    @IBOutlet weak var loginnBtn : UIButton?
    @IBOutlet weak var txtEmail  : UITextField?
    @IBOutlet weak var txtPassword  : UITextField?
    
    @IBAction func openSocialLinks(sender : UIButton)
    {
        var strUrl : String = ""
        if sender.tag == 1{  //facebook
            strUrl = Constants.URL_FACEBOOK
        }
        else if sender.tag == 2{ //Twitter
            strUrl = Constants.URL_TWITTER
        }
        else if sender.tag == 3{ //Youtube
            strUrl = Constants.URL_YOUTUBE
        }
        
        let url = NSURL(string: strUrl)!
        UIApplication.shared.openURL(url as URL)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var cRadius : CGFloat = 4.0
        
        if(GlobalStaticMethods.isPad())
        {
            cRadius = 6.0
        }

        txtEmail?.delegate = self
        txtPassword?.delegate = self
        
//        txtEmail?.text = "student@uod.edu.sa"
//        txtPassword?.text = "student"
        
        txtEmail?.text = "moqasem"
        txtPassword?.text = "ud@11223344"
        
        GlobalStaticMethods.makeRoundedEdges(view: userrView, radius: cRadius)
        GlobalStaticMethods.makeRoundedEdges(view: passsView, radius: cRadius)
        GlobalStaticMethods.makeRoundedEdges(view: loginnBtn, radius: cRadius)
        
        self.updateHeaderWithHeadingText(hText: "LOGIN", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:"", bgColor: Constants.COLOR_TOP_BAR)
    }
    
    @IBAction func loginUser(sender:UIButton?)
    {
        
        
        if(txtEmail?.text == "")
        {
            GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Please enter username", vc: self)
        }
        else if(txtPassword?.text == "")
        {
            GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Please enter password", vc: self)
        }
        else{
            let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                          Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                          "usr_user":txtEmail?.text ?? "",
                          "usr_pass":txtPassword?.text ?? ""]
            
            AFWrapper.serviceCallXml(strURL: Constants.URL_LDAP, soapAction:.ldap,  serviceName: Constants.SERVICE_VALID_USER, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
                
                print("DATA MSG  \(xml["soap:Envelope"]["soap:Body"])")
                let result = xml["soap:Envelope"]["soap:Body"]["UserValidResponse"]["UserValidResult"].element?.text
                
                if result?.lowercased() == "true"{
                    
                    self.getUserData()
                    
                }
                else{
                    GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Invalid username or password.", vc: self)
                }
            })
            { (NSError) in
                print(NSError)
                
                GlobalStaticMethods.showAlertWithTitle(title: "Error", message: "Error from server", vc: self)
            }
        }
        
//        if(txtEmail?.text == "student@uod.edu.sa" && txtPassword?.text == "student")
//        {
//            Singleton.sharedInstance.loginStatus = userLoginStatusEnum.kStudentLoggedIn
//            delegate.resetLeftMenu()
//            navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
//            
//        }
//        else if(txtEmail?.text == "employee@uod.edu.sa" && txtPassword?.text == "employee")
//        {
//            Singleton.sharedInstance.loginStatus = userLoginStatusEnum.kEmployeeLoggedIn
//            delegate.resetLeftMenu()
//            navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
//        }
//        else
//        {
//            GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Please enter valid student or employee email and passowrd", vc: self)
//        }
//        
//        txtEmail?.text = ""
//        txtPassword?.text = ""
    }
    
    func getUserData(){
        let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                      Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                      "usr_user":txtEmail?.text ?? ""]
        
        AFWrapper.serviceCallXml(strURL: Constants.URL_LDAP, soapAction:.ldap,  serviceName: Constants.SERVICE_GET_USER, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
            
            print("DATA MSG  \(xml["soap:Envelope"]["soap:Body"]["GetUserResponse"]["GetUserResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"])")
            
            /*
             <NewDataSet xmlns="">
             <Table1 diffgr:id="Table11" msdata:rowOrder="0" diffgr:hasChanges="inserted">
             <FullName>Mustafa mohammad Qasem</FullName>
             <IsLocked>0</IsLocked>
             <Phone>03-333</Phone>
             <Mobile>0561476122</Mobile>
             <ID>1212121212</ID>
             <Nationality />
             <BirthDate>27/12/1987</BirthDate>
             <BirthPlace />
             <SocialStatus />
             <Company>عمادة الإتصالات و تقنية المعلومات</Company>
             <Building>900,-</Building>
             </Table1>
             </NewDataSet>
            */
            
            Singleton.sharedInstance.userDetail = xml["soap:Envelope"]["soap:Body"]["GetUserResponse"]["GetUserResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
            
            
            Singleton.sharedInstance.loginStatus = userLoginStatusEnum.kEmployeeLoggedIn
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.resetLeftMenu()
            navigation.pushOrPop(viewControllerIdentifier: VC_DASHBOARD, sideDrawer: self.mm_drawerController)
            
        })
        { (NSError) in
            print(NSError)
            
            GlobalStaticMethods.showAlertWithTitle(title: "Error", message: "Error from server", vc: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
