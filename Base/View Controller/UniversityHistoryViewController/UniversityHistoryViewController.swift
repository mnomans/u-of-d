//
//  UniversityHistoryViewController.swift
//  Base
//
//  Created by noman  on 12/30/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.

import UIKit

class UniversityHistoryViewController: HeaderViewController {
    
    
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var contentView : UIView?
    @IBOutlet weak var sendBtnn: UIButton?
    var data : XMLIndexer?
    
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var txtDesc : BaseUITextView?
    @IBOutlet weak var imgTop : UIImageView?
    
    @IBOutlet  var txtViewhHeightConstraints : NSLayoutConstraint!
    
    var isFirstTime : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        isFirstTime = true
        
        
        if GlobalStaticMethods.isPhone(){
            self.txtViewhHeightConstraints?.constant = 170
            
            
        }else{
            self.txtViewhHeightConstraints?.constant = 250
            
        }
        sendBtnn?.addTarget(self, action:#selector(setHeight), for: .touchUpInside)
        
        
        // Do any additional setup after loading the view.
        
        sendBtnn?.layer.cornerRadius = 4.0;
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
        {
            sendBtnn?.layer.cornerRadius = 6.0;
        }
        
        sendBtnn?.layer.masksToBounds = true;
        
        self.updateHeaderWithHeadingText(hText: "UNIVERSITY HISTORY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                      Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                      "lang":"1"]
        
        AFWrapper.serviceCallXml(strURL: Constants.URL_FEATURE, soapAction:.feature,  serviceName: Constants.SERVICE_HISTORY, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
            
            print("DATA MSG  \(xml["soap:Envelope"]["soap:Body"])")
            self.data = xml["soap:Envelope"]["soap:Body"]["AboutUsResponse"]["AboutUsResult"]
            
            self.lblTitle?.text = self.data?["Title"].element?.text
            self.txtDesc?.setAttributedStringWithHtml(htmlStr: self.data?["Body"].element?.text ?? "")
            AFWrapper.LoadImage(strUrl: self.data?["Image"].element?.text ?? "", imgView: self.imgTop!, placeholder: "")
        })
        { (NSError) in
            print(NSError)
        }
    }
    
    func setHeight()  {
        
        if isFirstTime == true{
            
            txtViewhHeightConstraints.isActive = false
            isFirstTime = false
            sendBtnn?.setTitle("Read Less", for: .normal)
        }
        else{
            
            txtViewhHeightConstraints.isActive = true
            isFirstTime = true
            sendBtnn?.setTitle("Read More ", for: .normal)
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
