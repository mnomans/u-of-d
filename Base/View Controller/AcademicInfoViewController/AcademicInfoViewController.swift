//
//  AcademicInfoViewController.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class AcademicInfoViewController: HeaderViewController,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    var arrayInfo = [["title":"Primary Name", "value":"Kindergarden", "value2":"Status: Active in Program", "cellType": "dic"],
                     ["title":"Academic Group", "value":"College of Education Dammam", "value2":"Campus : Dammam City", "cellType": "dic"],
                     ["title":"Academic Level", "value":"Major Level 6", "value2":"", "cellType": "ic"],
                     ["title":"Academic Career", "value":"Undergraduate", "value2":"", "cellType": "ic"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //  collectionView.delegate = self
        // collectionView.dataSource = self
        
        self.updateHeaderWithHeadingText(hText: "ACADEMIC INFO", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
                
            }
        }
    }
    
    

    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = AcademicInfoCollectionViewCell()
        
        let data = arrayInfo[indexPath.row]
        
        if data["cellType"] == "dic"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dualInfoCell", for: indexPath as IndexPath) as! AcademicInfoCollectionViewCell
        }
        else
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "infoCell", for: indexPath as IndexPath) as! AcademicInfoCollectionViewCell
        }
        
        cell.lblName.text = data["title"]
        cell.lblValue.text = data["value"]
        cell.lblValue2?.text = data["value2"]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let data = arrayInfo[indexPath.row]
        
        // Set cell width to 100%
        
        
        if  GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro(){
                if data["cellType"] == "dic"{
                    return CGSize(width: collectionViewWidth - 40, height: 180)
                    
                }
                else{
                    return CGSize(width: collectionViewWidth - 40, height: 130)
                    
                }
            }
            else{
                
                if data["cellType"] == "dic"{
                    return CGSize(width: collectionViewWidth - 40, height: 170)
                    
                }
                else{
                    return CGSize(width: collectionViewWidth - 40, height: 130)
                    
                }
            }
            
        }
        else{
            
        if data["cellType"] == "dic"{
            return CGSize(width: collectionViewWidth - 40, height:110)
            
        }else{
            return CGSize(width: collectionViewWidth - 40, height: 80)
            
            }
        }
    }
}
