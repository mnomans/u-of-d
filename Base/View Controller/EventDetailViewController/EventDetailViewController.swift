//
//  EventDetailViewController.swift
//  Base
//
//  Created by noman  on 12/26/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class EventDetailViewController: HeaderViewController {

    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var imgTop : UIImageView?
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblTime : UILabel?
    @IBOutlet var lblDate : UILabel?
    @IBOutlet var lblLocation : UILabel?
    @IBOutlet var txtDesc : BaseUILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgTop?.image = nil
        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "EVENT DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        debugPrint("DATA \(Singleton.sharedInstance.eventDetail)")
        let data = Singleton.sharedInstance.eventDetail
        
        lblTitle?.text = data?["Title"].element?.text
        lblDate?.text = data?["Date"].element?.text
        lblTime?.text = data?["Time"].element?.text
        lblLocation?.text = data?["Location"].element?.text
        
        txtDesc?.setAttributedStringWithHtml(htmlStr: data?["Body"].element?.text ?? "")
        AFWrapper.LoadImage(strUrl: data?["Image"].element?.text ?? "", imgView: self.imgTop!, placeholder: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
