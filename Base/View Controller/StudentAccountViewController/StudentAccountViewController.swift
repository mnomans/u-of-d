//
//  StudentAccountViewController.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class StudentAccountViewController: HeaderViewController,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    var arrayInfo = [["title":"Fee Name", "value":"820", "cellType": "m"],
                     ["title":"Net Balance (SAR)", "value":"2100",  "cellType": "m2"],
                     ["title":"Change Date", "value":"", "cellType": "f"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //  collectionView.delegate = self
        // collectionView.dataSource = self
        
        self.updateHeaderWithHeadingText(hText: "STUDENT ACCOUNT", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = TermGradesCollectionViewCell()
        
        let data = arrayInfo[indexPath.row]
        
        if data["cellType"] == "f"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FooterCell", for: indexPath as IndexPath) as! TermGradesCollectionViewCell
        }
        else if data["cellType"] == "m2"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MidCell2", for: indexPath as IndexPath) as! TermGradesCollectionViewCell
        }
        else
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MidCell", for: indexPath as IndexPath) as! TermGradesCollectionViewCell
        }
        
        cell.lblTitle?.text = data["title"]
        cell.lblValue?.text = data["value"]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let data = arrayInfo[indexPath.row]
        
        if  GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro(){
                
            if data["cellType"] == "f"{

                return CGSize(width: collectionViewWidth - 40, height:80)
            }
            else if data["cellType"] == "m2"{
                return CGSize(width: collectionViewWidth - 40, height:60)
            }
            else{
                return CGSize(width: collectionViewWidth - 40, height: 80)
                
                }
            }
            else{
            
                if data["cellType"] == "f"{
                    
                    return CGSize(width: collectionViewWidth - 40, height:70)
                }
                else if data["cellType"] == "m2"{
                    return CGSize(width: collectionViewWidth - 40, height:50)
                }
                else{
                    return CGSize(width: collectionViewWidth - 40, height: 70)
                    
                }
            }
            
        }
        else{
            
        
        if data["cellType"] == "f"{
            return CGSize(width: collectionViewWidth - 40, height:50)
        }
        else if data["cellType"] == "m2"{
            return CGSize(width: collectionViewWidth - 40, height:30)
        }
        else{
            return CGSize(width: collectionViewWidth - 40, height: 50)
            
            }
        }
    }
}
