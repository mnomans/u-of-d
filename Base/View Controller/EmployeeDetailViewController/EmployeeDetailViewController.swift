//
//  EmployeeDetailViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 12/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class EmployeeDetailViewController: HeaderViewController ,UICollectionViewDelegate
,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    
    
    
    var arrayInfo = [["img": "ed-icon-name", "title":"Employee Name", "value":"Mantra Saad-Alsuble"],
                     ["img": "ed-icon-college", "title":"College/Deanship", "value":"College of Arts & Science"],
                     ["img": "ed-icon-office", "title":"Building/Office", "value":"900 / 2nd floor"],
                     ["img": "ed-icon-fax", "title":"Fax No", "value":"0096613330333"],
                     ["img": "ed-icon-email", "title":"Email", "value":"pr@uod.edu.sa"],
                     ["img": "ed-icon-phone", "title":"Extension No", "value":"3346"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.updateHeaderWithHeadingText(hText: "EMPLOYEE DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        debugPrint(Singleton.sharedInstance.employeeDetail)
    }


    override func viewWillAppear(_ animated: Bool) {
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()

    }
    
    
    
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = EmployeedDtailCollectionViewCell()
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! EmployeedDtailCollectionViewCell
        
        let data = arrayInfo[indexPath.row]
        cell.lblNameStat.text = data["title"]
        cell.lblNameService.text = data["value"]
        cell.imgView.image = UIImage(named: data["img"]!)

            // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let height = (self.collectionView.frame.height - 2) / 3
        
        
        // Set cell width to 100%
      
        
        //if constants.deviceType.IS_IPHONE_5{
        return CGSize(width: collectionViewWidth/2 - 0.5, height: height)
        //}else{
           //return CGSize(width: collectionViewWidth/2 - 0.5, height: height * 0.2035)
        //}
        
        
    }


}
