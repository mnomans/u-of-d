//
//  EnrollmentViewController.swift
//  Base
//
//  Created by noman  on 12/31/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit


enum  EnrollmentEnum: Int {
    case ViewClasses = 0,
    ShoppingCart,
    DropClass
}

class EnrollmentViewController: HeaderViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var dataArray = [["title":"View \nClasses", "img":"enrollment-icon-drop-class"],
                     ["title":"Shopping \nCart", "img":"enrollment-icon-shopping-cart"],
                     ["title":"Drop \nClasses", "img":"enrollment-icon-drop-class"]]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        self.updateHeaderWithHeadingText(hText: "ENROLLMENT", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let single = Singleton.sharedInstance
        single.classStatus = .kUnspecified
    }
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.dataArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashboardCollectionViewCell
        
        var data = dataArray[indexPath.row]
        cell.btnIcon.setImage(UIImage(named: data["img"]!), for: .normal)
        cell.lblBtn.text = data["title"];
        cell.btnIcon.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
        cell.btnIcon.tag = indexPath.row
        
        return cell
    }
    
    func btnClicked(sender: UIButton)
    {
        //write the task you want to perform on buttons click event..
        print("item at \(sender.tag)")
        
        let single = Singleton.sharedInstance
        let value = EnrollmentEnum(rawValue: sender.tag)
        switch  value{
            
        case .ViewClasses?:
            single.classStatus = .kAddClass
            navigation.goToViewController(viewControllerIdentifier: VC_SELECT_TERM, animation: true)
            break
            
        case .ShoppingCart?:
            navigation.goToViewController(viewControllerIdentifier: VC_SHOPPING_CART, animation: true)
            break
            
        case .DropClass?:
            single.classStatus = .kDropClass
            navigation.goToViewController(viewControllerIdentifier: VC_SELECT_TERM, animation: true)
            break
            
        default: break
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var aRatio : CGFloat = 1.4;
        var spacer : CGFloat = 0.0;
        
        if(GlobalStaticMethods.isPad())
        {
            aRatio = 1.29
            spacer = 10.0
        }
        
        let width  : CGFloat = (self.collectionView.frame.width-30)/3
        let height : CGFloat = (aRatio * width) - spacer
        
        return CGSize(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
