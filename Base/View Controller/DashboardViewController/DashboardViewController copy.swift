//
//  DashboardViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 12/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

enum  Dashboard: Int {
    case DRectorMsg = 0,
    DUniNews,
    DUniEvents,
    DUniHistory,
    DEmployeeDirectory,
    DMap,
    DUserLogin,
    DContactInfo,
    DSocialNetworks
}

enum  EDashboard: Int {
    case ERectorMsg = 0,
    EUniNews,
    EUniHistory,
    EUniEvents,
    EMap,
    EAdminServices,
    EICT,
    ECommunityServices,
    EAcademicServices,
    EEmployeeDirectory,
    ESocialNetworks,
    EContactInfo
    
}

enum  SDashboard: Int {
    case SProfile = 0,
    SAcademic,
    STimeTable,
    SGrades,
    SAttendance,
    SAccount,
    SEvents,
    SToDo,
    SAdvisors,
    SIndicators,
    SEnrollment,
    SAboutUni
}

class ViewController: HeaderViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var dataArray = [Dictionary<String,String>]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(Singleton.isStudentLoggedIn())
        {
            dataArray = [["title":"Profile", "img":"sdash-profile"],
                         ["title":"Academic", "img":"sdash-academic"],
                         ["title":"Time \nTable", "img":"sdash-timetable"],
                         ["title":"Grades", "img":"sdash-grades"],
                         ["title":"Attendance", "img":"sdash-attendance"],
                         ["title":"Account", "img":"sdash-account"],
                         ["title":"Events", "img":"sdash-events"],
                         ["title":"ToDo", "img":"sdash-todo"],
                         ["title":"Indicators", "img":"sdash-indicators"],
                         ["title":"Enrollment", "img":"sdash-enrollment"],
                         ["title":"About University", "img":"sdash-about-uni"]]
        }
        else if(Singleton.isEmployeeLoggedIn())
        {
            dataArray = [["title":"Rector's \nMessage", "img":"db-rector-msg"],
                         ["title":"University \nNews", "img":"db-news"],
                         ["title":"University \nHistory", "img":"db-history"],
                         ["title":"University \nEvents", "img":"db-events"],
                         ["title":"University \nMap", "img":"db-ma"],
                         ["title":"Administration \nServices", "img":"edash-icon-admin-services"],
                         ["title":"ICT \nServices", "img":"edash-icon-ict"],
                         ["title":"Community \nServices", "img":"edash-icon-community"],
                         ["title":"Academic \nServices", "img":"edash-icon-academic"],
                         ["title":"Employee \nDictionary", "img":"db-directory"],
                         ["title":"Social \nNetwork", "img":"db-social"],
                         ["title":"Contact \nDetail", "img":"db-information"]]
            
        }
        else
        {
            dataArray = [["title":"Rector's \nMessage", "img":"db-rector-msg"],
                         ["title":"University \nNews", "img":"db-news"],
                         ["title":"University \nEvents", "img":"db-events"],
                         ["title":"University \nHistory", "img":"db-history"],
                         ["title":"Employee \nDictionary", "img":"db-directory"],
                         ["title":"University \nMap", "img":"db-ma"],
                         ["title":"User \nLogin", "img":"db-login"],
                         ["title":"Contact \nInformation", "img":"db-information"],
                         ["title":"Social \nNetwork", "img":"db-social"]]
        }
        
        // Do any additional setup after loading the view.
        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        self.updateHeaderWithHeadingText(hText: "DASHBOARD", rightBtnImageName:"", leftBtnImageName:"", menuBtnImageName:"", bgColor: Constants.COLOR_TOP_BAR)
        
    }
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.dataArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashboardCollectionViewCell
        
        var data = dataArray[indexPath.row]
        cell.btnIcon.setImage(UIImage(named: data["img"]!), for: .normal)
        cell.lblBtn.text = data["title"];
        cell.btnIcon.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
        cell.btnIcon.tag = indexPath.row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var aRatio : CGFloat = 1.4
        var spacer : CGFloat = 0.0
        var variant: CGFloat = 50.0
        
        if(GlobalStaticMethods.isPad())
        {
            aRatio = 1.29
            spacer = 10.0
            variant = 30.0
        }
        
        let width  : CGFloat = (self.collectionView.frame.width-50)/3
        let height : CGFloat = (aRatio * width) - spacer
        
        return CGSize(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    func btnClicked(sender: UIButton)
    {
        //write the task you want to perform on buttons click event..
        print("item at \(sender.tag)")
        
        if(Singleton.isEmployeeLoggedIn())
        {
            let value = EDashboard(rawValue: sender.tag)
            switch  value{
                
            case EDashboard.ERectorMsg?:
                navigation.goToViewController(viewControllerIdentifier: VC_RECTOR_MSG, animation: true)
                break
                
            case EDashboard.EUniNews?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_NEWS, animation: true)
                break
                
            case EDashboard.EUniEvents?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_EVENTS, animation: true)
                break
                
            case EDashboard.EUniHistory?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_HISTORY, animation: true)
                break
                
            case EDashboard.EEmployeeDirectory?:
                navigation.goToViewController(viewControllerIdentifier: VC_EMPLOYEE_DIRECTORY, animation: true)
                break
                
            case EDashboard.EMap?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_MAP, animation: true)
                break
                
            case EDashboard.EAdminServices?:
                navigation.goToViewController(viewControllerIdentifier: VC_ADMIN_SERVICES, animation: true)
                break
                
            case EDashboard.EICT?:
                navigation.goToViewController(viewControllerIdentifier: VC_ICT_SERVICES, animation: true)
                break
                
            case EDashboard.EAcademicServices?:
                //navigation.goToViewController(viewControllerIdentifier: VC_ACADEMIC_INFO, animation: true)
                GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Coming soon in next version", vc: self)
                break
                
            case EDashboard.ECommunityServices?:
                //navigation.goToViewController(viewControllerIdentifier: VC_USER_LOGIN, animation: true)
                GlobalStaticMethods.showAlertWithTitle(title: "Message", message: "Coming soon in next version", vc: self)
                break
                
            case EDashboard.ESocialNetworks?:
                navigation.goToViewController(viewControllerIdentifier: VC_SOCIAL_NETWORK, animation: true)
                break
                
            case EDashboard.EContactInfo?:
                navigation.goToViewController(viewControllerIdentifier: VC_CONTACT_INFO, animation: true)
                break
                
                
            default: break
                
            }
        }
        else if(Singleton.isStudentLoggedIn())
        {
            let value = SDashboard(rawValue: sender.tag)
            switch  value{
                
            case SDashboard.SProfile?:
                navigation.goToViewController(viewControllerIdentifier: VC_PROFILE, animation: true)
                break
                
            case SDashboard.SAcademic?:
                navigation.goToViewController(viewControllerIdentifier: VC_ACADEMIC_INFO, animation: true)
                break
                
            case SDashboard.STimeTable?:
                //navigation.goToViewController(viewControllerIdentifier: VC_UNI_EVENTS, animation: true)
                break
                
            case SDashboard.SGrades?:
                navigation.goToViewController(viewControllerIdentifier: VC_GRADES, animation: true)
                break
                
            case SDashboard.SAttendance?:
                navigation.goToViewController(viewControllerIdentifier: VC_ATTENDANCE, animation: true)
                break
                
            case SDashboard.SAccount?:
                navigation.goToViewController(viewControllerIdentifier: VC_ACCOUNT, animation: true)
                break
                
            case SDashboard.SEvents?:
                navigation.goToViewController(viewControllerIdentifier: VC_EVENTS, animation: true)
                break
                
            case SDashboard.SToDo?:
                navigation.goToViewController(viewControllerIdentifier: VC_TODO, animation: true)
                break
                
            case SDashboard.SAdvisors?:
                navigation.goToViewController(viewControllerIdentifier: VC_ADVISOR, animation: true)
                break
                
            case SDashboard.SIndicators?:
                navigation.goToViewController(viewControllerIdentifier: VC_INDICATOR, animation: true)
                break
                
            case SDashboard.SEnrollment?:
                navigation.goToViewController(viewControllerIdentifier: VC_ENROLLMENT, animation: true)
                break
                
            case SDashboard.SAboutUni?:
                navigation.goToViewController(viewControllerIdentifier: VC_ABOUT_UNI, animation: true)
                break
                
                
            default: break
                
            }
        }
        else
        {
            let value = Dashboard(rawValue: sender.tag)
            switch  value{
                
            case Dashboard.DRectorMsg?:
                navigation.goToViewController(viewControllerIdentifier: VC_RECTOR_MSG, animation: true)
                break
                
            case Dashboard.DUniNews?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_NEWS, animation: true)
                break
                
            case Dashboard.DUniEvents?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_EVENTS, animation: true)
                break
                
            case Dashboard.DUniHistory?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_HISTORY, animation: true)
                break
                
            case Dashboard.DEmployeeDirectory?:
                navigation.goToViewController(viewControllerIdentifier: VC_EMPLOYEE_DIRECTORY, animation: true)
                break
                
            case Dashboard.DMap?:
                navigation.goToViewController(viewControllerIdentifier: VC_UNI_MAP, animation: true)
                break
                
            case Dashboard.DUserLogin?:
                navigation.goToViewController(viewControllerIdentifier: VC_USER_LOGIN, animation: true)
                break
                
            case Dashboard.DSocialNetworks?:
                navigation.goToViewController(viewControllerIdentifier: VC_SOCIAL_NETWORK, animation: true)
                break
                
            case Dashboard.DContactInfo?:
                navigation.goToViewController(viewControllerIdentifier: VC_CONTACT_INFO, animation: true)
                break
                
                
            default: break
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
