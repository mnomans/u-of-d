//
//  AccountDetailViewController.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class AccountDetailViewController: HeaderViewController,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    var arrayInfo = [["title":"Account Type", "value":"RUM", "cellType": "f"],
                     ["title":"Term", "value":"Academic year 2015-2016 Term 2", "cellType": "fns"],
                     ["title":"Amount", "value":"840", "cellType": "h"],
                     ["title":"Type", "value":"Payment", "cellType": "h"],
                     ["title":"Posted", "value":"05-02-16", "cellType": "fns"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //  collectionView.delegate = self
        // collectionView.dataSource = self
        
        self.updateHeaderWithHeadingText(hText: "ACCOUNT DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 20
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 10
                
            }
        }
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = PersonalInformationCollectionViewCell()
        
        let data = arrayInfo[indexPath.row]
        
        if data["cellType"] == "f"{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as! PersonalInformationCollectionViewCell
        }
        else if(data["cellType"] == "fns")
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCellNoSpace", for: indexPath as IndexPath) as! PersonalInformationCollectionViewCell
        }
        else{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! PersonalInformationCollectionViewCell
        }
        
        cell.lblName.text = data["value"]
        cell.lblInfoStat.text = data["title"]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let data = arrayInfo[indexPath.row]
        
        // Set cell width to 100%
        
        
        if  GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro(){
                if data["cellType"] == "f"{
                    return CGSize(width: collectionViewWidth - 40, height: 140)
                    
                }
                else if(data["cellType"] == "fns"){
                    return CGSize(width: collectionViewWidth - 40, height: 120) /// only this cell is in use
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 120)
                    
                }
            }
            else{
                
                if data["cellType"] == "f"{
                    return CGSize(width: collectionViewWidth - 40, height: 120)
                    
                }
                else if(data["cellType"] == "fns"){
                    return CGSize(width: collectionViewWidth - 40, height: 100)
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 100)
                    
                }
            }
            
        }
        else{
            if data["cellType"] == "f"{
            return CGSize(width: collectionViewWidth - 40, height: 80)
            
        }
        else if(data["cellType"] == "fns"){
            return CGSize(width: collectionViewWidth - 40, height: 60)
        }else{
            return CGSize(width: collectionViewWidth/2 - 20.5, height: 60)
            
            }
        }
    }
    
    
}
