//
//  SelectTermViewController.swift
//  Base
//
//  Created by noman  on 12/21/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class SelectTermViewController: HeaderViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView : UITableView?
    
    var dataArray = ["Term 1 2016", "Term 2 2016", "term 3 2017", "Term 4 2017", "Term 5 2018", "Term 6 2018", "Term 7 2019", "Term 8 2019"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "SELECT TERM", rightBtnImageName: "", leftBtnImageName: "gen-arrow-back", menuBtnImageName: "", bgColor: Constants.COLOR_TOP_BAR)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let single = Singleton.sharedInstance
        var controller : String = VC_GRADES
        
        if single.classStatus != .kUnspecified {
            controller = VC_SELECT_SUBJECT
        }
        
         navigation.goToViewController(viewControllerIdentifier: controller, animation: true)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "select-term-cell", for: indexPath) as! SelectTermTableViewCell
        
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL_ALTERNATE)
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL);
        }
        
        cell.lblBtn?.text = dataArray[indexPath.row]
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
