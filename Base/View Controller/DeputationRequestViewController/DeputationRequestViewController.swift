//
//  DeputationRequestViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 16/01/2017.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit

class DeputationRequestViewController: HeaderViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView : UIWebView?
    @IBOutlet weak var indicator : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let username = (MMDrawerController.encryptData("moqasem").base64EncodedString())
        let strUrl = "\(Constants.URL_DEPUTATION_REQUEST)&usr=\(username)&lang=en"
        
        print(strUrl)
        
        webView?.loadRequest(URLRequest(url: URL(string: strUrl)!))
        webView?.delegate = self
        webView?.scrollView.bounces = false
        
        self.updateHeaderWithHeadingText(hText: "DEPUTATION REQUEST", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView)
    {
        indicator?.isHidden = false
        indicator?.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        indicator?.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
