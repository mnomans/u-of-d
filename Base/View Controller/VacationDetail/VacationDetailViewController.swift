//
//  VacationDetailViewController.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 10/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import  SwiftyJSON
class VacationDetailViewController: HeaderViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    var arrayInfo = ["Employee Name","Employee Number","Actual","Available","Excluded","Taken","Total","Type","Type ID","Directive Date","Directive No","Leave Days","Leave End Date","Leave Strat Date","Leave Type Code","Period From","Period To"]
    var arrdata = ["Employee Name","Employee Number","Actual","Available","Excluded","Taken","Total","Type","Type ID","Directive Date","Directive No","Leave Days","Leave End Date","Leave Strat Date","Leave Type Code","Period From"]
    
    
  
    
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    @IBOutlet  var topHeightConstraint: NSLayoutConstraint?
    
    var dataArray : JSON?
    var index : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.updateHeaderWithHeadingText(hText: "VACATION DETAIL", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        setDataforDetail()

    }

    override func viewWillAppear(_ animated: Bool) {
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
   
    // MARK: - UICollectionViewDataSource protocol
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
       // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = VacationDetailCollectionViewCell()
        
        if indexPath.row == 0{
            let headerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as! VacationDetailCollectionViewCell
            
            headerCell.lblName.text = dataArray?["EmpFullName"].stringValue
            headerCell.lblInfoStat.text = self.arrayInfo[indexPath.row ]

            return headerCell

            
        }
        else{
        
        
         cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! VacationDetailCollectionViewCell
            
            cell.lblInfoStat.text = self.arrayInfo[indexPath.row ]
            cell.lblName.text = arrdata[indexPath.row - 1]

            
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        }
        return cell
    }

     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width

        
        // Set cell width to 100%
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if indexPath.row == 0{
                    return CGSize(width: collectionViewWidth - 40, height: 200)
                    
                }else{
                    return CGSize(width: collectionViewWidth/2 - 20.5, height: 150)
                    
                }
            }
            else{
                
            
            if indexPath.row == 0{
                return CGSize(width: collectionViewWidth - 40, height: 160)
                
            }else{
                return CGSize(width: collectionViewWidth/2 - 20.5, height: 100)
                
            }

            }
        }else{
         
            if indexPath.row == 0{
                return CGSize(width: collectionViewWidth - 40, height: 100)
                
            }else{
                return CGSize(width: collectionViewWidth/2 - 20.5, height: 60)
                
            }
            
            
        }
        
        
    }

    func setDataforDetail()  {
        
        arrdata[0] = (dataArray?["EmpNumber"].stringValue)!
        arrdata[1] = (dataArray?["Actual"].stringValue)!
        arrdata[2] = (dataArray?["Available"].stringValue)!
        arrdata[3] = (dataArray?["Excluded"].stringValue)!
        arrdata[4] = (dataArray?["Taken"].stringValue)!
        arrdata[5] = (dataArray?["Total"].stringValue)!
        arrdata[6] = (dataArray?["Type"].stringValue)!
        arrdata[7] = (dataArray?["TypeId"].stringValue)!
        arrdata[8] = (dataArray?["VacationInfoList"][index!]["DirectiveDate"].stringValue)!
        arrdata[9] = (dataArray?["VacationInfoList"][index!]["DirectiveNo"].stringValue)!
        arrdata[10] = (dataArray?["VacationInfoList"][index!]["LeaveDays"].stringValue)!
        arrdata[11] = (dataArray?["VacationInfoList"][index!]["LeaveEndDate"].stringValue)!
        arrdata[12] = (dataArray?["VacationInfoList"][index!]["LeaveStartDate"].stringValue)!
        arrdata[13] = (dataArray?["VacationInfoList"][index!]["LeaveTypeCode"].stringValue)!
        arrdata[14] = (dataArray?["VacationInfoList"][index!]["PeriodFrom"].stringValue)!
        arrdata[15] = (dataArray?["VacationInfoList"][index!]["PeriodTo"].stringValue)!
        
    }
    
    
    
    
}
