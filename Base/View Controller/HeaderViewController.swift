//
//  HeaderViewController.swift
//  Skeleton
//
//  Created by Waqas Ali on 12/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//


import UIKit

public class HeaderViewController: UIViewController, HeaderViewDelegate {
    
    var _headerView:HeaderView!
    
    internal var viewsDictionary = Dictionary<String,AnyObject>()
    internal var constraints = [NSLayoutConstraint]()
    var headerViewHeight : CGFloat = 56
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        headerViewHeight = GlobalStaticMethods.getHeaderFooterHeight()
      //  self.loadWithNib("HeaderView", viewIndex: 0, owner: self)
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HeaderView", bundle: bundle)
        self._headerView = nib.instantiate(withOwner: self, options: nil)[0] as! HeaderView
        
        self.view.addSubview(self._headerView!)
        
        _headerView.delegate = self

        configureHeaderViewConstraints()
        
        //self .updateHeaderWithHeadingText(hText: "", rightBtnImageName: "", leftBtnImageName:"", menuBtnImageName: "", bgColor: "#113c8b")
        

        let myfont = self._headerView.lblHeading.font
        
        
        
        //
        //        var fontSize = 0
        //        var ratio = 0
        if GlobalStaticMethods.isPhone() {
            self._headerView.lblHeading.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: "Roboto-Bold", psdFontSize:56)
            
            //            fontSize = 10 // PUt psd font size for iphone
            //            ratio = fontSize/1242
            //
        }
        else if GlobalStaticMethods.isPadPro(){
            self._headerView.lblHeading.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: "Roboto-Bold", psdFontSize:56)
            
            //            fontSize = 10 // PUt psd font size for pro
            //
            //            ratio = fontSize/2048
            //
        }
        else{
            self._headerView.lblHeading.font = myfont!.setGeneralFontSizeUsingRatio(nameFont: "Roboto-Bold", psdFontSize:42)
            
            //            fontSize = 10 // PUt psd font size for pro
            //
            //            ratio = fontSize/1536
            //
        }

        
      
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadWithNib(nibName:String, viewIndex:Int, owner: AnyObject) -> Void {
        
        let headerNib : UIView = Bundle.main.loadNibNamed(nibName, owner: owner, options: nil)?.first as! UIView
        
//        let headerNib : UIView = (Bundle.mainBundle.loadNibNamed(nibName, owner: owner, options: nil) as NSArray).objectAtIndex(viewIndex) as! UIView;
        
        
        headerNib.frame = CGRect( x : 0, y:  0, width :  self.view.frame.width, height : 56)
        
       self.view.addSubview(headerNib)
        
       
    }
    
    
    
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, menuBtnImageName: String, bgColor: String  ) -> Void {
        
        
        _headerView.updateHeaderWithHeadingText(hText: hText, rightBtnImageName: rightBtnImageName, leftBtnImageName: leftBtnImageName, menuBtnImageName: menuBtnImageName, bgColor: bgColor)
        
    }

    func headerViewLeftBtnDidClick(headerView: HeaderView) {
   
        print("leftButtonClicked")
        self.navigationController!.popViewController(animated: true)
    }
    
    
    func headerViewRightBtnDidClick(headerView: HeaderView) {
        print("rightButtonClicked")
    }
    
    func headerViewMenuBtnDidClick(headerView: HeaderView) {
            mm_drawerController .toggle(.left, animated: true) { (success) in
            print("Allhamdulillah")
        }
        
    }
    
    
    func configureHeaderViewConstraints(){
        
        
        viewsDictionary["headerView"] = _headerView
        
        _headerView.translatesAutoresizingMaskIntoConstraints = false
        
        let strVConstraints = "V:|-0-[headerView(\(headerViewHeight))]"
        let strHConstraints = "H:|-0-[headerView]-0-|"
        
        setConstraintsForHeader(format: strVConstraints)
        setConstraintsForHeader(format: strHConstraints)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func setConstraintsForHeader(format : String){
        
        let newConstraint = NSLayoutConstraint.constraints(withVisualFormat: format, options:[], metrics: nil, views: viewsDictionary)
        
        constraints += newConstraint
        
    }
    

    override public var prefersStatusBarHidden: Bool {
        return true
    }  
    
    
    
}
