//
//  ContactUsViewController.swift
//  
//
//  Created by Mohammed Noman Siddiqui on 12/12/2016.
//
//

import UIKit

class ContactUsViewController: HeaderViewController ,UICollectionViewDelegate
    ,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    //@IBOutlet var lblheading1 :
    
    var dataXml : XMLIndexer?
    var arrayInfo = [["img": "cu-icon-dept", "title":"Department", "value":"Public Relations"],
                     ["img": "cu-icon-phone", "title":"Phone", "value":"0096613333376"],
                     ["img": "cu-icon-email", "title":"Email", "value":"pr@uod.edu.sa"],
                     ["img": "cu-icon-fax", "title":"Fax", "value":"0096613330333"]]
    
    @IBOutlet weak var collectionView: UICollectionView! // We make it weak for memory consideration
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblContact.text = "Phone:013-3331111 \nEmail:helpdesk@udd.edu.sa"
        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "CONTACT US", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                      Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                      "lang":"1"]
        
        AFWrapper.serviceCallXml(strURL: Constants.URL_FEATURE, soapAction:.feature,  serviceName: Constants.SERVICE_CONTACT_INFO, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
            
            print("DATA MSG  \(xml["soap:Envelope"]["soap:Body"])")
            self.dataXml = xml["soap:Envelope"]["soap:Body"]["PresidentMessageResponse"]["PresidentMessageResult"]
        })
        { (NSError) in
            print(NSError)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.arrayInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        var cell  = ContactUsCollectionViewCell()
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! ContactUsCollectionViewCell
        
        let data = arrayInfo[indexPath.row]
        
        cell.lblNameStat.text = data["title"]
        cell.lblNameService.text = data["value"]
        cell.imgView.image = UIImage(named: data["img"]!)
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = self.collectionView.bounds.size.width
        
        let height = (self.collectionView.frame.height / 2) - 0.5
        
        
        // Set cell width to 100%
        
        return CGSize(width: collectionViewWidth/2 - 0.5, height: height)
        
    }
    
}
