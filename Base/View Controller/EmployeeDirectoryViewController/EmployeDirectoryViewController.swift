//
//  EmployeDirectoryViewController.swift
//  Base
//
//  Created by noman  on 12/17/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class EmployeDirectoryViewController: HeaderViewController {

    @IBOutlet var groupViewHeightConstraints : NSLayoutConstraint?
    @IBOutlet weak var btnSearch : UIButton?
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    
    @IBOutlet weak var lblDesc : UILabel?
    @IBOutlet weak var lblDesc2 : UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "EMPLOYEE DIRECTORY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        lblDesc?.text = "Please add 33 from the extension, \nExample 33 (333)"
        lblDesc2?.text = "For Calling from outside of Dammam \nPlease add 01333 before the extension, Example 1033 (33333)"
    }

    
   

    
    @IBAction func search(sender:UIButton!)
    {
        navigation.goToViewController(viewControllerIdentifier: "ESearchResultViewController", animation: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPhone() {
            let height = self.view.frame.size.height
            
            self.groupViewHeightConstraints?.constant = height * 0.058
            
        }else{
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()

        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
