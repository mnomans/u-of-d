//
//  HelloViewController.swift
//  Base
//
//  Created by Hamza Khan on 24/11/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import AMLocalized
class HelloViewController: HeaderViewController {

    @IBOutlet var leftMenuButton : UIButton?
    @IBOutlet var rightMenuButton : UIButton?
    @IBOutlet var changeLanguageButton : UIButton?
    @IBOutlet var lblWelcomeText : UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _headerView.heading = "Login"
//        _headerView.lblHeading.fontName = "Helvetica"
        _headerView.bgColorVar = "#543178"

        
        
        leftMenuButton?.addTarget(self, action: #selector(openLeftMenu), for: .touchUpInside)
        rightMenuButton?.addTarget(self, action: #selector(openRightMenu), for: .touchUpInside)
        changeLanguageButton?.addTarget(self, action: #selector(selectLanguage), for: .touchUpInside)
        
        
        
        // Do any additional setup after loading the view.
        //--ww serviceCall()
        
        let number = 8
        let myArray = ["a","b","c"]
        let myDict = [ "a":"1" , "b":"2", "c":"3"]
        
        printLog("this is a string")
    
        
        printLog("this is a \(number)")
     
        
        printLog("this is a variable ", number)
        
        
        printLog("this is array", myArray)
        
        
        printLog("this is dict", myDict)
       
        
        
        
       
       
      
       
 
    }
    override func viewWillAppear(_ animated: Bool) {
        lblWelcomeText?.text = LocalizationSystem.sharedLocal().localizedString(forKey: "Welcome", value: "Welcome")

    }
    
    func selectLanguage(){
        let alert = UIAlertController(title: "Choose language", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "English", style: .default) { (action) in
            print("English")
            language.setLanguage(type: ChooseLanguageTypeEnum.kLanguageEnglish)
            
            navigation.goToViewController(viewControllerIdentifier: VC_DASHBOARD, animation : true)
        }
        let action2 = UIAlertAction(title: "Arabic", style: .default) { (action) in
            print("Arabic")
            language.setLanguage(type: ChooseLanguageTypeEnum.kLanguageArabic )
            navigation.goToViewController(viewControllerIdentifier: VC_DASHBOARD , animation : true)

        }
        let action3 = UIAlertAction(title: "Urdu", style: .default) { (action) in
            print("Urdu")
            language.setLanguage(type: ChooseLanguageTypeEnum.kLanguageUrdu)
            navigation.goToViewController(viewControllerIdentifier: VC_DASHBOARD , animation : true)


        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        self.present(alert, animated: true) { 
            print("Hello Alert Controller")
            
        }
    }
    func openLeftMenu(){
        
        mm_drawerController.toggle(.left, animated: true) { (success) in
            
        }
    }
    func openRightMenu(){
        
        mm_drawerController.toggle(.right, animated: true) { (success) in
            
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func serviceCall() {
        
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.LoginUserDotNet(userName: "sanjay", password: "1234"), success: { (JSON) in
            printLog(JSON)
            }) { (NSError) in
              print(NSError)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
