//
//  SocialNetworkViewController.swift
//  Base
//
//  Created by noman  on 12/26/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class SocialNetworkViewController: HeaderViewController {

    @IBOutlet var topHeightConstraint : NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "SOCIAL NETWORKS", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    @IBAction func openSocialLinks(sender : UIButton)
    {
        var strUrl : String = ""
        if sender.tag == 1{  //facebook
            strUrl = Constants.URL_FACEBOOK
        }
        else if sender.tag == 2{ //Twitter
            strUrl = Constants.URL_TWITTER
        }
        else if sender.tag == 3{ //Youtube
            strUrl = Constants.URL_YOUTUBE
        }
        
        let url = NSURL(string: strUrl)!
        UIApplication.shared.openURL(url as URL)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
