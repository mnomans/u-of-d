//
//  OvertimeViewController.swift
//  Base
//
//  Created by noman  on 12/31/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import SwiftyJSON

class OvertimeViewController: HeaderViewController ,UITableViewDataSource ,UITableViewDelegate{
    
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var tblView : UITableView?
    
    var dataArray : JSON?
    
   // var arr =
    
    var arrData : [String]?
    var arrDataStatic : Array<String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        serviceCall()

        arrData = ["","","","",""]
        
        arrDataStatic = ["Employee No","Employee Name","Basic Salary","Transportation","Rank"]
        
        tblView?.estimatedRowHeight = 140
        tblView?.rowHeight = UITableViewAutomaticDimension
        tblView?.separatorColor = UIColor (hexString: "F1ECD9")
        
        tblView?.preservesSuperviewLayoutMargins = false
        tblView?.separatorInset = UIEdgeInsets.zero
        tblView?.layoutMargins = UIEdgeInsets.zero
        
        
        self.updateHeaderWithHeadingText(hText: "OVERTIME INQUIRY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func serviceCall()  {
        
        AFWrapper.requestPOSTURL(Constants.baseURL + Constants.SERVICE_OVERTIME, params: ["userName":"Asfaifi"], headers: Constants.headerArray, success: { (JSON) in
            debugPrint("DATA \(JSON)")
            
           // self.dataArray = JSON.arrayValue as! [AnyObject]
            self.dataArray = JSON["d"]
            print("JSON: \(self.dataArray)")
            self.setDataInArray()
            self.tblView?.reloadData()
            
        }) { (Error) in
            debugPrint("Error \(Error)")
        }
        
    }
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setDataInArray()  {
       
        let str  = dataArray?["EmpNumber"].stringValue
        let str1  = dataArray?["EmpFullName"].stringValue
        let str2  = dataArray?["Basic"].stringValue
        let str3 = dataArray?["Transportation"].stringValue
        let str4  = dataArray?["Rank"].stringValue
        
//        self.arrData![0] = str!
//        self.arrData![1] = str1!
//        self.arrData![2] = str2!
//        self.arrData?[3] = str3!
//        self.arrData?[4] = str4!

        arrData?[0...4] = [str!,str1!,str2!,str3!,str4!]
        
        self.tblView?.delegate = self
        self.tblView?.dataSource = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 50
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 30
                
            }
        }
    }
    
    
    //  MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! OverTimeTableViewCell
        
        headerCell.headerLbl?.text = "Summary"
        
        return headerCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
        
    }
    
    
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if section == 0 {
                    return 0
                    
                }else{
                    
                    return 130
                }
                
            }else{
                
                if section == 0 {
                    return 0
                    
                }
                
                
                return 100
            }
            
            
        }
            
            
        else{
            if section == 0 {
                return 0
                
            }
            else{
                
                return 70
            }
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return (arrData?.count)!
        }
        else{
            
            if (dataArray?["OvertimeInfoList"] .count)! > 0{
                return (dataArray!["OvertimeInfoList"] .count + 1)

            }else{
                return 0
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = OverTimeTableViewCell()
        
        if indexPath.section == 0{
            
            cell = tableView.dequeueReusableCell(withIdentifier: "CellWithOutSpace", for: indexPath) as! OverTimeTableViewCell
            cell.empLbl.text = arrDataStatic?[indexPath.row]
            cell.empValuelLbl.text = arrData?[indexPath.row]
            
        }
        else {
            
            if(indexPath.row == 0)
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! OverTimeTableViewCell
            }
            else{
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell4Label", for: indexPath) as! OverTimeTableViewCell
                
                cell.netLbl.text = dataArray?["OvertimeInfoList"][indexPath.row - 1]["Net"].stringValue
                cell.NoOfdaysLbl.text = dataArray?["OvertimeInfoList"][indexPath.row - 1]["NumberofDays"].stringValue
                cell.PayrollLbl.text = dataArray?["OvertimeInfoList"][indexPath.row - 1]["PayrollNo"].stringValue
                cell.transportLbl.text = dataArray?["OvertimeInfoList"][indexPath.row - 1]["Transport"].stringValue

                
                
                if indexPath.row % 2 == 0{
                    
                    cell.netLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.NoOfdaysLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.PayrollLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.transportLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    
                    
                }else{
                    
                    cell.netLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.NoOfdaysLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.PayrollLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.transportLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    
                    
                }
                
                
                
            }
            
            
            
            // cell.configureCell(cell: cell, forRowAtIndexPath: indexPath as NSIndexPath, lblArray: arrData!)
            
        }
        
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if indexPath.section == 0 {
                    return 120
                    
                }else{
                    
                    return 60
                }
                
            }else{
                
                if indexPath.section == 0 {
                    return 100
                    
                }
                
                
                return 50
            }
            
            
        }
            
            
        else{
            if indexPath.section == 0{
                return 60
            }
            else{
                return 30
            }
        }
    }
}
