//
//  EmpDeputationInquiryViewController.swift
//  Base
//
//  Created by noman  on 12/23/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import SwiftyJSON

class EmpDeputationInquiryViewController: HeaderViewController ,UITableViewDataSource ,UITableViewDelegate{
    
    @IBOutlet var empNumber : UILabel!
    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var tblView : UITableView?
    var dataArray : JSON?
    
    // var arr =
    
    var arrData : [String]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceCall()
        
        arrData = ["","","","",""]

        
        
        tblView?.estimatedRowHeight = 140
        tblView?.rowHeight = UITableViewAutomaticDimension
        tblView?.separatorColor = UIColor (hexString: "F1ECD9")
        
        tblView?.preservesSuperviewLayoutMargins = false
        tblView?.separatorInset = UIEdgeInsets.zero
        tblView?.layoutMargins = UIEdgeInsets.zero

        
        
        self.updateHeaderWithHeadingText(hText: "DEPUTATION INQUIRY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
      
        
        // Do any additional setup after loading the view.
    }
    
    func serviceCall()  {
        
        AFWrapper.requestPOSTURL(Constants.baseURL + Constants.SERVICE_DEPUTATIONINQUIRY, params: ["userName":"Asfaifi"], headers: Constants.headerArray, success: { (JSON) in
            debugPrint("DATA \(JSON)")
            
            // self.dataArray = JSON.arrayValue as! [AnyObject]
            self.dataArray = JSON["d"]
            print("JSON: \(self.dataArray)")
            
            self.tblView?.delegate = self
            self.tblView?.dataSource = self
            self.tblView?.reloadData()
            
            self.empNumber.text = self.dataArray?["EmpNumber"].stringValue
            
            
        }) { (Error) in
            debugPrint("Error \(Error)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
                
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 50
                
            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 30
                
            }
        }
    }
    
    
    //  MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! DeputationInquiryTableViewCell
        
        headerCell.headerLbl?.text = "Summary"
        
        return headerCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
        
    }
    
    
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if section == 0 {
                    return 0
                    
                }else{
                    
                    return 130
                }
                
            }else{
                
                if section == 0 {
                    return 0
                    
                }
                
                
                return 100
            }
            
            
        }
        
        
        else{
        if section == 0 {
            return 0
            
        }
        else{
        
        return 70
            }
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return 1
        }
        else{
            if (dataArray?["DeputationInfoList"] .count)! > 0{
                return (dataArray!["DeputationInfoList"] .count + 1)
                
            }else{
                return 0
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = DeputationInquiryTableViewCell()

        if indexPath.section == 0{
            
            
            cell = tableView.dequeueReusableCell(withIdentifier: "CellWithOutSpace", for: indexPath) as! DeputationInquiryTableViewCell
            
            cell.empValuelLbl.text = dataArray?["EmpFullName"].stringValue
            
           
        }
        else {
            
            
            
            
            if(indexPath.row == 0)
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! DeputationInquiryTableViewCell
            }
            else{
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell4Label", for: indexPath) as! DeputationInquiryTableViewCell
                
                
                cell.DesicionLbl.text = dataArray?["DeputationInfoList"][indexPath.row - 1]["DecisionNo"].stringValue
                cell.NoOfdaysLbl.text = dataArray?["DeputationInfoList"][indexPath.row - 1]["NumberofDays"].stringValue
                cell.PayrollLbl.text = dataArray?["DeputationInfoList"][indexPath.row - 1]["PayrollNo"].stringValue

                
                if indexPath.row % 2 == 0{
                                    
                    cell.DetailView.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.NoOfdaysLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.PayrollLbl.backgroundColor = UIColor (hexString: "fcf9f2")
                    cell.DesicionLbl.backgroundColor = UIColor (hexString: "fcf9f2")

                    
                    
                }else{
                    
                    
                    cell.DetailView.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.NoOfdaysLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.PayrollLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    cell.DesicionLbl.backgroundColor = UIColor (hexString: "fdfeff")
                    
                    
                    
                    
                }

                
                
            }
            
           
            cell.DetailBtn?.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
            cell.DetailBtn?.tag = indexPath.row
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func btnClicked(sender: UIButton)
    {
        print("item at \(sender.tag)")
        

        let viewController = storyboard?.instantiateViewController(withIdentifier: VC_DEPUTATION_DETAIL) as! DeputationDetailViewController
        viewController.dataArray = self.dataArray
        viewController.index = sender.tag - 1
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
      //  navigation.goToViewController(viewControllerIdentifier: VC_DEPUTATION_DETAIL, animation: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
                
                if indexPath.section == 0 {
                    return 140
                    
                }else{
                    
                    return 60
                }
                
            }else{
                
                if indexPath.section == 0 {
                    return 100
                    
                }
                
                
                return 50
            }
            
            
        }
        
        
        else{
        if indexPath.section == 0{
                return 60
                    }
        else{
            return 30
        }
        }
    }
}
