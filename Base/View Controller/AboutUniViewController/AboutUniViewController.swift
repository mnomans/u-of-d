//
//  AboutUniViewController.swift
//  Base
//
//  Created by noman  on 12/17/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

enum  AboutUniEnum: Int {
    case RectorMsg = 0,
    UniNews,
    UniEvents,
    UniHistory,
    Map,
    ContactInfo
}


class AboutUniViewController: HeaderViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var dataArray = [["title":"Rector's \nMessage", "img":"db-rector-msg"],
                     ["title":"University \nNews", "img":"db-news"],
                     ["title":"University \nEvents", "img":"db-events"],
                     ["title":"University \nHistory", "img":"db-history"],
                     ["title":"University \nMap", "img":"db-ma"],
                     ["title":"Contact \nInformation", "img":"db-information"]]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        self.updateHeaderWithHeadingText(hText: "ABOUT UNIVERSITY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // return 4
        return self.dataArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashboardCollectionViewCell
        
        var data = dataArray[indexPath.row]
        cell.btnIcon.setImage(UIImage(named: data["img"]!), for: .normal)
        cell.lblBtn.text = data["title"];
        cell.btnIcon.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
        cell.btnIcon.tag = indexPath.row
        
        return cell
    }
    
    func btnClicked(sender: UIButton)
    {
        //write the task you want to perform on buttons click event..
        print("item at \(sender.tag)")
        
        let value = AboutUniEnum(rawValue: sender.tag)
        switch  value{
            
        case .RectorMsg?:
            navigation.goToViewController(viewControllerIdentifier: VC_RECTOR_MSG, animation: true)
            break
            
        case .UniNews?:
            navigation.goToViewController(viewControllerIdentifier: VC_UNI_NEWS, animation: true)
            break
            
        case .UniEvents?:
            navigation.goToViewController(viewControllerIdentifier: VC_UNI_EVENTS, animation: true)
            break
            
        case .UniHistory?:
            navigation.goToViewController(viewControllerIdentifier: VC_UNI_HISTORY, animation: true)
            break
            
        case .Map?:
            navigation.goToViewController(viewControllerIdentifier: VC_UNI_MAP, animation: true)
            break
            
        case .ContactInfo?:
            navigation.goToViewController(viewControllerIdentifier: VC_CONTACT_INFO, animation: true)
            break
            
        default: break
            
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var aRatio : CGFloat = 1.4
        var spacer : CGFloat = 0.0
        var variant: CGFloat = 50.0
        
        if(GlobalStaticMethods.isPad())
        {
            aRatio = 1.23
            spacer = 57.0
            variant = 35.0
        }
        
        let width  : CGFloat = (self.collectionView.frame.width-50)/3
        let height : CGFloat = (aRatio * width) - spacer
        
        return CGSize(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
