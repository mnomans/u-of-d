//
//  NewsViewController.swift
//  Base
//
//  Created by noman  on 12/17/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class NewsViewController: HeaderViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView : UITableView?
    var dataXml : XMLIndexer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblView?.estimatedRowHeight = 100
        tblView?.rowHeight = UITableViewAutomaticDimension
        
        self.updateHeaderWithHeadingText(hText: "UNIVERSITY NEWS", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                      Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                      //"courseId" : "23"
                     "count":Constants.COUNT_UNLIMITED,
                     "lang":"1"]
        
        AFWrapper.serviceCallXml(strURL: Constants.URL_FEATURE, soapAction:.feature,  serviceName: Constants.SERVICE_FEEDS, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
            
            print("DATA FEEDS  \(xml["soap:Envelope"]["soap:Body"]["FeedsResponse"]["FeedsResult"]["Feed"])")
            self.dataXml = xml["soap:Envelope"]["soap:Body"]["FeedsResponse"]["FeedsResult"]["Feed"]
            self.tblView?.reloadData()
        })
        { (NSError) in
            print(NSError)
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataXml?.children.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "News-Cell", for: indexPath) as! NewsTableViewCell
        
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL_ALTERNATE)
            cell.bgArrow?.backgroundColor = UIColor(hexString: Constants.COLOR_MAROON)
            cell.imgArrow?.image = UIImage(named: "gen-arrow-right")
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL)
            cell.bgArrow?.backgroundColor = UIColor(hexString: Constants.COLOR_YELLOW)
            cell.imgArrow?.image = UIImage(named: "gen-arrow-blue")
        }
        
        let data = dataXml?[indexPath.row]
        cell.lblHeading?.text = data?["Title"].element?.text?.deleteHTMLTag(tag: "") //data["title"]
        cell.lblDesc?.text = data?["Body"].element?.text?.deleteHTMLTag(tag: "") //data["desc"]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.animateCell(cell)
    }
    
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        Singleton.sharedInstance.newsDetail = dataXml?[indexPath.row]
        navigation.goToViewController(viewControllerIdentifier: VC_NEWS_DETAIL, animation: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
