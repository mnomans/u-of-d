//
//  VocationInquiryViewController.swift
//  Base
//
//  Created by noman  on 12/17/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit
import SwiftyJSON

class VocationInquiryViewController: HeaderViewController ,UITableViewDataSource ,UITableViewDelegate{

    @IBOutlet var topHeightConstraint : NSLayoutConstraint?
    @IBOutlet var empNumber : UILabel?

    @IBOutlet var tblView : UITableView?
    var dataArray : JSON?
    
    
    var arrData : [String]?
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView?.estimatedRowHeight = 140
        tblView?.rowHeight = UITableViewAutomaticDimension
        tblView?.separatorColor = UIColor.clear
        serviceCall()
        
        arrData = ["","Balance Detail","Summary"]
        // Do any additional setup after loading the view.
        
        self.updateHeaderWithHeadingText(hText: "VACATION INQUIRY", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalStaticMethods.isPad(){
            if GlobalStaticMethods.isPadPro(){
               
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 50

            }else{
                topHeightConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 30

            }
        }
    }
    
    
    func serviceCall()  {
        
        AFWrapper.requestPOSTURL(Constants.baseURL + Constants.SERVICE_VICATIONINQUIRY, params: ["userName":"Asfaifi"], headers: Constants.headerArray, success: { (JSON) in
            debugPrint("DATA \(JSON)")
            
            // self.dataArray = JSON.arrayValue as! [AnyObject]
            self.dataArray = JSON["d"]
            print("JSON: \(self.dataArray)")
            
            self.empNumber?.text = self.dataArray?["EmpNumber"].stringValue
            
            self.tblView?.delegate = self
            self.tblView?.dataSource = self
            self.tblView?.reloadData()
            
            
            
        }) { (Error) in
            debugPrint("Error \(Error)")
        }
        
    }

    
   //  MARK: - Table view data source
    
    
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! VocationInquiryTableViewCell
    
            headerCell.headerLbl?.text = arrData?[section];

            return headerCell
        }
        
        func numberOfSections(in tableView: UITableView) -> Int{
            return 3
            
        }
    
    
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        
        if GlobalStaticMethods.isPad(){
            
            if GlobalStaticMethods.isPadPro() {
              
                if section == 0 {
                    return 0
                    
                }else{
                
                return 130
            }

            }else{
               
                if section == 0 {
                    return 0
                    
                }
                
                
                return 100
            }

                
        }else{
        if section == 0 {
            return 0
  
        }
        else{
        
        return 60
            }
        }

    }
    

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return 2
        }else if section == 1
        {
            return 3
        }
        else{
            if (dataArray?["VacationInfoList"] .count)! > 0{
                return (dataArray!["VacationInfoList"] .count + 1)
                
            }else{
                return 0
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = VocationInquiryTableViewCell()
        cell.selectionStyle = .none

        if indexPath.section == 0{
        
        if(indexPath.row == 0)
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellWithSpace", for: indexPath) as! VocationInquiryTableViewCell
            
            cell.empValueLbl.text = dataArray?["EmpFullName"].stringValue
            
        }
        else
        {

            cell = tableView.dequeueReusableCell(withIdentifier: "Cell2Lbl", for: indexPath) as! VocationInquiryTableViewCell
            
            cell.typeValueLbl.text = dataArray?["Type"].stringValue
            cell.madeValueLbl.text = dataArray?["TypeId"].stringValue

            cell.typeLbl.text = "Holiday Type"
            cell.madeLbl.text = "Holiday Mode"

            
        }
        }else if indexPath.section == 1{
            if(indexPath.row == 0)
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell2Lbl", for: indexPath) as! VocationInquiryTableViewCell
                
                cell.typeValueLbl.text = dataArray?["Total"].stringValue
                cell.madeValueLbl.text = dataArray?["Excluded"].stringValue
                
                cell.typeLbl.text = "Total Balance"
                cell.madeLbl.text = "Excluded Balance"

                
            }
            else if(indexPath.row == 1)
            {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell2Lbl", for: indexPath) as! VocationInquiryTableViewCell
                
                cell.typeValueLbl.text = dataArray?["Actual"].stringValue
                cell.madeValueLbl.text = dataArray?["Taken"].stringValue
                
                cell.typeLbl.text = "Actual Balance"
                cell.madeLbl.text = "Taken Balance"
                
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: "CellWithOutSpace", for: indexPath) as! VocationInquiryTableViewCell

                
                
                cell.balanceLbl.text = "Available Balance"
                cell.balanceValueLbl.text = dataArray?["Available"].stringValue
            }
        }
            else {
                if(indexPath.row == 0)
                {
                    cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! VocationInquiryTableViewCell
                }
                else{
            
                    cell = tableView.dequeueReusableCell(withIdentifier: "Cell4Label", for: indexPath) as! VocationInquiryTableViewCell
                    
                    cell.periodToLbl.text = dataArray?["VacationInfoList"][indexPath.row - 1]["PeriodTo"].stringValue
                    cell.periodFormLbl.text = dataArray?["VacationInfoList"][indexPath.row - 1]["PeriodFrom"].stringValue
                    cell.leaveDayLbl.text = dataArray?["VacationInfoList"][indexPath.row - 1]["LeaveDays"].stringValue

                    
                    cell.btnDetail?.addTarget(self, action: #selector(self.btnClicked(sender:)), for: .touchUpInside)
                    cell.btnDetail?.tag = indexPath.row

                }
            
        }
        
        
        return cell
    }

    func btnClicked(sender: UIButton)
    {
     
        print("item at \(sender.tag)")

        let viewController = storyboard?.instantiateViewController(withIdentifier: VC_VACATION_DETAIL) as! VacationDetailViewController
        viewController.dataArray = self.dataArray
        viewController.index = sender.tag - 1
        self.navigationController?.pushViewController(viewController, animated: true)
        
    //    navigation.goToViewController(viewControllerIdentifier: VC_VACATION_DETAIL, animation: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if GlobalStaticMethods.isPad(){
          
            if GlobalStaticMethods.isPadPro(){
               
                if indexPath.section == 0{
                    if indexPath.row == 0{
                        return 140
                    }else{
                        return 120
                        
                    }
                }
                else if indexPath.section == 2{
                    
                    return 60
                }
                else{
                    
                    return 120
                }
                
                
            }
            else{
               
                if indexPath.section == 0{
                    if indexPath.row == 0{
                        return 120
                    }else{
                        return 100
                        
                    }
                }
                else if indexPath.section == 2{
                    
                    return 50
                }
                else{
                    
                    return 100
                }
                
                
            }
            
            
        }
        
        else{
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 80
            }else{
                return 60

            }
        }
        else if indexPath.section == 2{
            
            return 30
        }
        else{
            
            return 60
            }
        }
        
    }
    
}
