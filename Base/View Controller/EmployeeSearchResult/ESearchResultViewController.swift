//
//  ESearchResultViewController.swift
//  Base
//
//  Created by noman  on 12/17/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class ESearchResultViewController: HeaderViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView : UITableView?
    var dataXml : XMLIndexer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tblView?.estimatedRowHeight = 100
        tblView?.rowHeight = UITableViewAutomaticDimension
        
        self.updateHeaderWithHeadingText(hText: "EMPLOYEE LIST", rightBtnImageName:"", leftBtnImageName:Constants.LEFT_BUTTON_IMAGE, menuBtnImageName:Constants.MENU_BUTTON_IMAGE, bgColor: Constants.COLOR_TOP_BAR)
        
        let params = [Constants.KEY_GEN_USERNAME : Constants.GEN_USERNAME,
                      Constants.KEY_GEN_PASSWORD : Constants.GEN_PASSWORD,
                      "text": "k",
                      "category": "-1",
                      "lang": "1"]
        
        AFWrapper.serviceCallXml(strURL: Constants.URL_FEATURE, soapAction:.feature, serviceName: Constants.SERVICE_EMP_SEARCH, method: Constants.METHOD_POST, params: params as Dictionary<String, AnyObject>, success: { (xml) in
            
            print("DATA EVENTS  \(xml["soap:Envelope"]["soap:Body"])")
            self.dataXml = xml["soap:Envelope"]["soap:Body"]["IPTeleResponse"]["IPTeleResult"]["IPTele"]
            self.tblView?.reloadData()
        })
        { (NSError) in
            print(NSError)
        }

    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataXml?.children.count ?? 0;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ESearch-Cell", for: indexPath) as! ESarchResultTableViewCell
        
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL_ALTERNATE)
            cell.bgArrow?.backgroundColor = UIColor(hexString: Constants.COLOR_MAROON)
            cell.imgArrow?.image = UIImage(named: "gen-arrow-right")
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(hexString: Constants.COLOR_CELL)
            cell.bgArrow?.backgroundColor = UIColor(hexString: Constants.COLOR_YELLOW)
            cell.imgArrow?.image = UIImage(named: "gen-arrow-blue")
        }
        
        let data = dataXml?[indexPath.row]
        
        cell.lblHeading?.text = data?["Name"].element?.text ?? "Name : NA"
        
        var dept = data?["Department"].element?.text ?? ""
        
        if dept == "" {
            dept = "Department : NA"
        }
        
        let email = data?["Email"].element?.text ?? ""
        //let location = data?["Location"].element?.text ?? ""
        
        cell.lblDesc?.text = "\(dept) \n\(email)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.animateCell(cell)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        Singleton.sharedInstance.employeeDetail = dataXml?[indexPath.row]
        navigation.goToViewController(viewControllerIdentifier:"EmployeeDetailViewController", animation: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
