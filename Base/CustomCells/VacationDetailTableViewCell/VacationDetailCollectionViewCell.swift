//
//  VacationDetailCollectionViewCell.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 11/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class VacationDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblInfoStat : UILabel!

}
