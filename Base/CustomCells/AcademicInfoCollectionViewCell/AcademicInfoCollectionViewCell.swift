//
//  AcademicInfoCollectionViewCell.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class AcademicInfoCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblValue : UILabel!
    @IBOutlet var lblValue2 : UILabel?
}
