//
//  VocationInquiryTableViewCell.swift
//  
//
//  Created by noman  on 12/17/16.
//
//

import UIKit

class VocationInquiryTableViewCell: UITableViewCell {

    
    @IBOutlet var headerLbl : UILabel!
    
    @IBOutlet var periodFormLbl : UILabel!
    @IBOutlet var periodToLbl : UILabel!
    @IBOutlet var leaveDayLbl : UILabel!
    @IBOutlet weak var btnDetail : UIButton?

    @IBOutlet var empLbl : UILabel!
    @IBOutlet var empValueLbl : UILabel!

    @IBOutlet var typeLbl : UILabel!
    @IBOutlet var typeValueLbl : UILabel!

    @IBOutlet var madeLbl : UILabel!
    @IBOutlet var madeValueLbl : UILabel!
    
    @IBOutlet var balanceLbl : UILabel!
    @IBOutlet var balanceValueLbl : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
