//
//  SelectTermTableViewCell.swift
//  Base
//
//  Created by noman  on 12/21/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class SelectTermTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBtn : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
