//
//  NewsTableViewCell.swift
//  Base
//
//  Created by noman  on 12/17/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeading : UILabel?
    @IBOutlet weak var lblDesc : UITextView?
    @IBOutlet weak var bgArrow : UIView?
    @IBOutlet weak var imgArrow : UIImageView?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblDesc?.textContainer.maximumNumberOfLines = 3
        lblDesc?.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
