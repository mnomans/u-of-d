//
//  CourseTableViewCell.swift
//  Base
//
//  Created by noman  on 1/14/17.
//  Copyright © 2017 Hamza Khan. All rights reserved.
//

import UIKit

class CourseTableViewCell: UITableViewCell {
    
    @IBOutlet var headerLbl : UILabel!
    
    @IBOutlet var headernameLbl : UILabel!
    @IBOutlet var headerdurationLbl : UILabel!
    @IBOutlet var headerlocationLbl : UILabel!
    @IBOutlet var headercpointLbl : UILabel!
    
    @IBOutlet var empLbl : UILabel!
    @IBOutlet var empValuelLbl : UILabel!
    
    
    @IBOutlet var nameLbl : UILabel!
    @IBOutlet var durationLbl : UILabel!
    @IBOutlet var locationLbl : UILabel!
    @IBOutlet var cpointLbl : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(cell: UITableViewCell, forRowAtIndexPath: NSIndexPath, lblArray: Array<String>) {
        
        let index = forRowAtIndexPath.row
        //  headerLbl.text = lblArray[index]
        
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
