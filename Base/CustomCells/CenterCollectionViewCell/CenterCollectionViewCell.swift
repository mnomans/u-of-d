//
//  CenterCollectionViewCell.swift
//  TimeTable
//
//  Created by TRA-MBP02 on 1/9/17.
//  Copyright © 2017 TRA-MBP02. All rights reserved.
//

import UIKit

class CenterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblClass : UILabel?
    @IBOutlet weak var btnDetail : UIButton?
    
}
