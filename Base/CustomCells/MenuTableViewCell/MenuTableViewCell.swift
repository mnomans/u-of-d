//
//  MenuTableViewCell.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 15/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBtn : UILabel?
    @IBOutlet weak var rowIcon : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
