//
//  DashboardCollectionViewCell.swift
//  Base
//
//  Created by Mohammed Noman Siddiqui on 13/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class DashboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnIcon : UIButton!
    @IBOutlet weak var lblBtn : UILabel!
    
}
