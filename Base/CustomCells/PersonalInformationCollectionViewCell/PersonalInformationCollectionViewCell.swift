//
//  PersonalInformationCollectionViewCell.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class PersonalInformationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblInfoStat : UILabel!
}
