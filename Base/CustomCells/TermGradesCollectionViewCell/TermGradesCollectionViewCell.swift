//
//  TermGradesCollectionViewCell.swift
//  Base
//
//  Created by noman  on 12/24/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class TermGradesCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblValue : UILabel?
}
