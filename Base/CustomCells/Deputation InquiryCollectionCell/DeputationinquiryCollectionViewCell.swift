//
//  DeputationinquiryCollectionViewCell.swift
//  
//
//  Created by Mohammed Noman Siddiqui on 12/12/2016.
//
//

import UIKit

class DeputationinquiryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblNameStat : UILabel!
    @IBOutlet var lblNameService : UILabel!
    @IBOutlet var lblDicision : UILabel!
    @IBOutlet var lblPayroll : UILabel!
    @IBOutlet var lblNoOfDays : UILabel!
    @IBOutlet var lblDetail : UILabel!
    
    @IBOutlet weak var BtnDetail : UIButton?
}
