//
//  SideTableViewCell.swift
//  TimeTable
//
//  Created by TRA-MBP02 on 1/9/17.
//  Copyright © 2017 TRA-MBP02. All rights reserved.
//

import UIKit

class SideTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
