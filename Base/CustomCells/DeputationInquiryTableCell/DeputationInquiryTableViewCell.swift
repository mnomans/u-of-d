//
//  DeputationInquiryTableViewCell.swift
//  Base
//
//  Created by noman  on 12/23/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class DeputationInquiryTableViewCell: UITableViewCell {

    @IBOutlet var headerLbl : UILabel!

    @IBOutlet var headerDesicionLbl : UILabel!
    @IBOutlet var headerPayrollLbl : UILabel!
    @IBOutlet var headerNoOfdaysLbl : UILabel!
    @IBOutlet var headerDetailLbl : UILabel!

    @IBOutlet var DesicionLbl : UILabel!
    @IBOutlet var PayrollLbl : UILabel!
    @IBOutlet var NoOfdaysLbl : UILabel!
    @IBOutlet var DetailBtn : UIButton?
    @IBOutlet var DetailView : UIView!
    
    @IBOutlet var empLbl : UILabel!
    @IBOutlet var empValuelLbl : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(cell: UITableViewCell, forRowAtIndexPath: NSIndexPath, lblArray: Array<String>) {
        
        let index = forRowAtIndexPath.row
      //  headerLbl.text = lblArray[index]
        
        if index == 0{
            
            DetailView.backgroundColor = UIColor (hexString: "fcf9f2")
            NoOfdaysLbl.backgroundColor = UIColor (hexString: "fcf9f2")
            PayrollLbl.backgroundColor = UIColor (hexString: "fcf9f2")
            DesicionLbl.backgroundColor = UIColor (hexString: "fcf9f2")
            
            
            
        }else{
            
            
            DetailView.backgroundColor = UIColor (hexString: "fdfeff")
            NoOfdaysLbl.backgroundColor = UIColor (hexString: "fdfeff")
            PayrollLbl.backgroundColor = UIColor (hexString: "fdfeff")
            DesicionLbl.backgroundColor = UIColor (hexString: "fdfeff")
            
            
            
            
        }
    }

    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
