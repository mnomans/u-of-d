//
//  SalarySlipTableViewCell.swift
//  Base
//
//  Created by noman  on 12/23/16.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import UIKit

class SalarySlipTableViewCell: UITableViewCell {

    @IBOutlet var ViewCell : UIView!

    @IBOutlet var AllowanceNameLBL : UILabel!
    @IBOutlet var AllowanceAmountLBL : UILabel!
    
    @IBOutlet var HeaderAllowanceNameLBL : UILabel!
    @IBOutlet var HeaderAllowanceAmountLBL : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(cell: UITableViewCell, forRowAtIndexPath: NSIndexPath, lblArray: Array<String>) {
        
        let index = forRowAtIndexPath.row
        //  headerLbl.text = lblArray[index]
        
        if index == 0{
            
            AllowanceNameLBL.backgroundColor = UIColor (hexString: "fcf9f2")
            AllowanceAmountLBL.backgroundColor = UIColor (hexString: "fcf9f2")
            
        }else{
            
            
            AllowanceNameLBL.backgroundColor = UIColor (hexString: "fdfeff")
            AllowanceNameLBL.backgroundColor = UIColor (hexString: "fdfeff")
            
        }
    }
    
    

}
